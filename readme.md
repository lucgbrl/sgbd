## Sistema de Gerenciamento Bibliotecario.

# 1 - Instalar php7.1 e mysql5.7
###### Base ubuntu 16.04
```
#PHP
sudo add-apt-repository ppa:ondrej/php
sudo apt update
sudo apt install php7.1 php7.1-cli php7.1-common php7.1-json php7.1-opcache php7.1-mysql php7.1-mbstring php7.1-mcrypt php7.1-zip php7.1-fpm php7.1-curl php7.1-xml php7.1-gd php7.1-ctype php7.1-tokenizer
#mysql
sudo apt install mysql-server
```
Crie a database que você irá utilizar

```
mysql -uusername -p 
 
create schema databasename 
```
# 2 - Instalação composer
###### Caso já tenha instalado pule para o passo 3
```
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer

```
# 3 - Gere a APP_KEY
Execute-o na raiz do projeto, dentro da pasta sgbd
```
php artisan key:generate   
```
# 4 - Arquivo .env
Abra o arquivo .env.example dentro da raiz do projeto e edite-o colocando suas configurações
Depois renomei-o para .env

```
#Nome API KEY
APP_KEY=
#Configurações da sua database
DB_CONNECTION=mysql
DB_HOST=localhost
DB_PORT=3306
DB_DATABASE= nomedadatabase
DB_USERNAME= usuario
DB_PASSWORD= senha

#Configurações de Email

MAIL_HOST=
MAIL_PORT=
MAIL_USERNAME=
MAIL_PASSWORD=
MAIL_ENCRYPTION=null

```

# 5 Comandos Laravel
```
#Na raíz do projeto onde se encontram as pastas app, resource, public etc execute os seguintes comandos
composer install #Vai instalar as dependencias
php artisan key:generate # Gera a API KEY
php artisan migrate #Vai instalar as tabelas do banco
php artisan db:seed --class=AdminTableSeeder #cadastra o Administrador
#caso não queira adicionar mais dados por comando siga para o ultimo comando
php artisan db:seed --class=BiblioTableSeeder #cadastra o Bibliotecario
php artisan db:seed --class=CursosTableSeeder #cadastra os Cursos UFC-Sobral
php artisan db:seed --class=CategoriasTableSeeder #cadastra as Categorias de Livros
php artisan db:seed --class=AutoresTableSeeder #cadastra alguns autores de Livros
php artisan db:seed --class=LivrosTableSeeder #cadastra alguns Livros
#caso queira adicionar dados fakes de usuários
./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/Unit/ProfessorTest.php
./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/Unit/FuncionariosTest.php
./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/Unit/AlunosTest.php

php artisan serve #inicia o servidor

```

#####Criado por Mateus Malveira
