<div class="form-group row">
    <label for="matricula" class="col-md-4 col-form-label text-md-right">{{ __('Matricula:') }}</label>

    <div class="col-md-6">
        <input data-mask="000000" data-mask-selectonfocus="true" id="matricula" type="text" class=" matricula form-control{{ $errors->has('matricula') ? ' is-invalid' : '' }}" mask='000000' name="matricula" value="{{ old('matricula') }}" required>        
    </div>
</div>
<div class="form-group row">
    <label class="col-md-4 col-form-label text-md-right" for="curso">Curso: </label>
    <div class="col-md-6">
    <select  name="curso" data-live-search="true" class="form-control"  id="curso">
        <option selected disabled  value="">Escolha</option>
        @foreach($cursos as $c)
        <option value="{{$c->cod_curso}}">{{$c->nome_curso}}</option>
        @endforeach          
    </select>
    </div>
</div>