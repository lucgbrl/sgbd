@extends ('layouts.admin')
@section ('conteudo')

@include('layouts.errors')

   <div class="card">
    <div class="card-header">

      <!-- Search Form -->
      <div class="form-row">
        <div class="col-md-4 py-1">
          <!-- name field -->
          <i class="fas fa-list"></i>Livros
        </div>
        <div class="col-md-4 ">
            @if(Auth::guard('admin')->check())
            <div class="text-center">
            <a href="/admin/livros/create"><button class="btn btn-outline-color ">Incluir</button></a>
            </div>
            @endif
        </div>
        <div class="col-md-4">
          @if(Auth::guard('biblio')->check())
          @include ('layouts.search',['search'=>route('livrosbiblio.index')])  
          @elseif(Auth::guard('admin')->check())
          @include ('layouts.search',['search'=>route('livrosadmin.index')]) 
          @else                 
          @include ('layouts.search',['search'=>route('livros.index')])      
          @endif
        </div>
      </div>

    </div>
    <div class="card-body wsc">
      <!-- Table-->
      <table class="table table-hover table-responsive">
        <thead>
          <tr>
            <!-- Modificar o cabeçalho de acordo com a tela-->
            <th>#</th>
            <th>ISBN</th>
            <th>Título</th>
            <th>Ano</th>
            <th>Editora</th>
            <th>Categoria</th>
            <th>Total</th>
            <th>Disponíveis</th>
            <th>Autores</th>
            @if(Auth::guard('admin')->check())
            <th>Ações</th>
            @endif
          </tr>
        </thead>
        <tbody>
            <!-- Table Body - só basta colocar 1 campo como exemplo-->
            @foreach ($livros as $key=>$lvr)
            <tr>
               <td>{{ ++$key }}</td>
               <td>{{ $lvr->ISBN }}</td>
               <td style='width:40%;'>{{ $lvr->titulo }}</td>
               <td>{{ $lvr->ano_lancamento }}</td>
               <td>{{ $lvr->editora }}</td>
               <td>{{ $lvr->categoria }}</td>
               <td>{{ $lvr->total }}</td>
               <td>{{ $lvr->disponiveis }}</td>               
               <td>{{ $lvr->autor }}</td>
                  @if(Auth::guard('admin')->check())
               <td style='width:12%;'>
                 {{-- Buttons Edit and Delete--}}
                  @include('layouts.buttons-index',['routeedit'=>'LivrosController@edit', 'ids'=>$lvr->ISBN])
               </td>
                  @endif
            </tr>
               @endforeach
            </tbody>
         </table>
         @foreach ($livros as $key=>$lvr) 
            @include('layouts.modals.modal-delete',[
                'nametype'=>'Livro',
                'ids'=>$lvr->ISBN,
                'namefield'=>$lvr->titulo,              
                'action'=>'LivrosController@destroy'
            ])
        @endforeach

      </div>

   </div>
   <!-- Paginação -->
   {{$livros->links('layouts.pagination')}}

@endsection