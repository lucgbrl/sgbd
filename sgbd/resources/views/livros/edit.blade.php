@extends ('layouts.admin')
@section ('conteudo')

<!-- Errors -->
    @include ('layouts.errors')  
    <div class="card">
        <div class="card-header">
            <!--Name Field -->
            <i class="fas fa-pencil-alt "></i>Editar Livros:  {{ $livro->titulo }}
        </div>
        <div class="card-body">
            {!! Form::model($livro, ['method'=>'PATCH', 'route'=>['livros.update',$livro->ISBN]]) !!}
            {{ Form::token() }}
            <div class='form-row'>
                <div class="form-group col-sm-3">
                  <label for="ISBN">ISBN: </label>
                <input type="text" max="13" min="13" disabled value="{{$livro->ISBN}}" name="ISBN" id="ISBN" class="form-control">
                </div>
                <div class="form-group col-sm-9">
                  <label for="titulo">Título: </label>
                  <input type="text" name="titulo" value="{{$livro->titulo}}" id="titulo" class="form-control">
                </div>                          
                <div class="form-group col-sm-2">
                  <label for="lançamento">Ano de lançamento: </label>
                  <input type="number" min="1000" value="{{$livro->ano_lancamento}}" name="lancamento" id="lançamento" class="form-control">
                </div>
                <div class="form-group col-sm-2">
                  <label for="editora">Editora: </label>
                  <input type="text" name="editora" value="{{$livro->editora}}" id="editora" class="form-control">
                </div>
                <div class="form-group col-sm-2">
                    <label for="copias">Categoria: </label><br>
                    <select id='' data-live-search="true" class="custom-select" name='categorias' style="width:100%">
                        <!-- foreach to list category selected -->
                        @foreach ( $categoria as $ct )
                            @if ( $livro->id_categoria == $ct->id_categoria)
                                <option value="{{$livro->id_categoria}}" selected> {{$ct->nome}}  </option>
                            @endif
                        @endforeach
                        @foreach ( $categoria as $ct )
                            @if ( $livro->id_categoria != $ct->id_categoria)
                                <option value="{{$ct->id_categoria}}" selected> {{$ct->nome}}  </option>
                            @endif
                        @endforeach    
                      </select>   
                </div>                
                <div class="form-group col-sm-5">
                        <label for="autores">Autores: </label><br>
                        <select style="width:100%" id='autores' multiple   data-live-search="true" class="selectpicker" name='autores[]' style="width:100%">                              
                            @foreach($autoreslivros as $lha)
                            <option selected value="{{$lha->cpf}}" >{{$lha->nome}}</option>
                            @endforeach
                            {{--Outros autores --}}
                            @foreach($outros as $lwa)
                            <option value="{{$lwa->cpf}}" >{{$lwa->nome}}</option>
                            @endforeach
                            
                        </select>

                        
                    </div>
                <div class="form-group col-sm-3">
                  <label for="copias">Total de cópias: </label>
                  <input type="number" value="{{$livro->quantidade_copias}}" name="copias" id="copias" class="form-control">
                </div>                           
              </div>



            <!--.. Aqui-->
            <!--buttons-->            
                @include ('layouts.update-buttom')            
            {!! Form::close() !!}
        </div>
    </div>

@endsection