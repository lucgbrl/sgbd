<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{asset('img/opt/icon.svg')}}" type="img/svg">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Sistema de Gerenciamento de Bibliotecas') }}</title>

    <!-- Scripts -->    
    <script type="text/javascript" src="{{asset('js/plugins.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/main.js')}}"></script>     

    <!-- Fonts -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light white home-page" id="navbar">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">
                <img class="icon-color" src="{{asset('img/opt/icon.svg')}}" title="Icon" class="d-inline-block" width="40em" alt="">
                <h1 class="d-inline-block">
                    {{ config('app.name', 'Sistema de Gerenciamento de Bibliotecas') }}</h1>
                <br>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse text-uppercase" id="navbarNavDropdown">
                <div class="ml-auto">
                    <ul id="menu-menu-tags" class="nav navbar-nav">
                <!-- Authentication Links -->
                @guest
                    <li><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
                    <li><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>

</div>
</nav>

        <section class="content">
            @yield('content')
        </section>
         {{--Tirar Depois--}}
    <script type="text/javascript" src="{{asset('js/main/mask-custom.js')}}"></script>
    </div>
</body>
</html>
