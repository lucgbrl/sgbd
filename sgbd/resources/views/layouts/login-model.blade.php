<section class="login">
<div class="container">        
    <div class="row">
        <div class="col-xl-5 col-lg-6 col-md-10 col-sm-12 mx-auto mt-lg-5">
            <div class="card">
                <div class="card-header">{{ __("Login $type") }}</div>
                <div class="card-body">
                    <form method="POST" action="{{ route("$route") }}">
                        @csrf

                        <div class="form-group">
                            <i class="fas fa-envelope prefix"></i>
                            <label for="email">{{ __('E-Mail Address') }}</label>
                           
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif                           
                        </div>

                        <div class="form-group">
                            <i class="fas fa-lock prefix"></i>
                            <label for="password">{{ __('Password') }}</label>
                            
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif                            
                        </div>

                        <div class="form-group">
                            <div class="text-center">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group ">
                            <div class="text-center">
                                <button type="submit" class="btn btn-color">
                                    {{ __('Login') }}
                                </button>                               
                            </div>
                            <div class="text-center">
                                <a class="btn btn-link" href="{{ route("$rec") }}">
                                    {{ __('Esqueceu a Senha?') }}
                                </a>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>