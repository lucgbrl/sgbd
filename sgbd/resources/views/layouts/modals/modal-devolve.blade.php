<div class="modal fade" id="{{'devolve'.$ids}}" tabindex="-1" role="dialog" aria-labelledby="#{{'title'.$ids}}" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      {{Form::open(array('route' => array("$action", $ids), 'method' => 'delete'))}}
      <div class="modal-content">
        <div class=" text-white modal-header bg-warning">
        <h6 class="modal-title" id="{{'title'.$ids}}">{{$nametype}} {{ $namefield }}</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div> 
        <div class="modal-body">
          <h6 class="text-capitalize">{{ $userty }}: {{ $useral }}
          <h6>Livro: {{$namefield}} </h6>
          </h6>
        <h6 id='multa{{$ids}}' class=" py-3 mt-3 h4 text-center bg-danger text-white">Multa Gostosa</h6>
        </div>                   
        <div class="modal-footer">
          <div class="text-center">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-color">{{$buttonname}}</button>
        </div>  
        </div>
        {{Form::Close()}}
      </div>
    </div>
  </div>
  <script>
    var data1<?php echo($ids) ?> = new Date("<?php echo date('Y-m-d H:i:s');?>");  
    var data2<?php echo($ids) ?> = new Date("<?php echo $datedv; ?>");
    // diferença de datas
    var days<?php echo($ids); ?> = parseInt((data2<?php echo($ids); ?>-data1<?php echo($ids); ?>)/(24*3600*1000));
  //se negativo entao
    if((days<?php echo($ids); ?>) < 0){
      //multiplica por menos -1
      days<?php echo($ids); ?>=days<?php echo($ids); ?> * -1;
      //multiplicando pelo valor da multa
      var multa<?php echo($ids); ?> = (parseFloat(days<?php echo($ids); ?>).toFixed(3) * parseFloat(<?php echo $multa;?>).toFixed(3));
      //Jogando a multa para dentro da variavel
      $('#multa{{$ids}}').html("Multa "+days<?php echo($ids); ?>+" Dias"+"</br>"+"R$"+multa<?php echo("$ids"); ?> +'0');
    }
    else{
      $('#multa{{$ids}}').addClass('d-none');

    }   
     
     
  </script>