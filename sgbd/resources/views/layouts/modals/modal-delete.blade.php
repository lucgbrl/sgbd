<div class="modal fade" id="{{'delete'.$ids}}" tabindex="-1" role="dialog" aria-labelledby="#{{'title'.$ids}}" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      {{Form::Open(array('action'=>array($action,$ids),'method'=>'delete'))}}
      <div class="modal-content">
        <div class="modal-header bg-danger">
        <h6 class="modal-title" id="{{'title'.$ids}}">Deletar {{$nametype}} {{ $namefield }}</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>                    
        <div class="modal-footer">
          <div class="text-center">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-color">Deletar</button>
        </div>  
        </div>
        {{Form::Close()}}
      </div>
    </div>
  </div>