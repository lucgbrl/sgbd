{{-- Esta sessão é só para tester--}}
@if(Auth::guard('web')->check())
<p class="text-success">
Está logado como User  {{ Auth::user()->name }}
</p>
@else
<p class="text-danger">
 Não está logado como User 
</p>
@endif
@if(Auth::guard('admin')->check())
<p class="text-success">
Está logado como Admin  {{ Auth::user()->name }}
</p>
@else
<p class="text-danger">
 Não está logado como Admin 
</p>
@endif
@if(Auth::guard('biblio')->check())
<p class="text-success">
Está logado como Biblio  {{ Auth::user()->name }}
</p>
@else
<p class="text-danger">
 Não está logado como Biblio 
</p>
@endif
