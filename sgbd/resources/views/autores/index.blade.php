@extends ('layouts.admin')
@section ('conteudo')

@include('layouts.errors')

   <div class="card">
    <div class="card-header">

      <!-- Search Form -->
      <div class="form-row">
        <div class="col-md-4 py-1">
          <!-- name field -->
          <i class="fas fa-list"></i>Autores
        </div>
        <div class="col-md-4 ">
            @if(Auth::guard('admin')->check())
            <div class="text-center">
            <a href="/admin/autores/create"><button class="btn btn-outline-color ">Incluir</button></a>
            </div>
            @endif
        </div>
        <div class="col-md-4">
          @if(Auth::guard('admin')->check())
            @include ('layouts.search',['search'=>'/admin/autores'])
          @elseif(Auth::guard('biblio')->check())
            @include ('layouts.search',['search'=>'/biblio/autores'])
          @else
            @include ('layouts.search',['search'=>'/autores'])
          @endif
        </div>
      </div>

    </div>
    <div class="card-body wsc">
      <!-- Table-->
      <table class="table table-hover table-responsive">
        <thead>
          <tr>
            <!-- Modificar o cabeçalho de acordo com a tela-->
            <th>#</th>
            <th>Nome</th>
            <th>Nacionalidade</th>
            <th>Ações</th>
          </tr>
        </thead>
        <tbody>
            <!-- Table Body - só basta colocar 1 campo como exemplo-->
            @foreach ($autor as $key=>$a)
            <tr>
               <td>{{ ++$key }}</td>
               <td>{{ $a->nome }}</td>
               <td>{{ $a->nacionalidade }}</td>
               <td>
                 {{-- Buttons Edit and Delete--}}
                 @if(Auth::guard('biblio')->check())
                 <a class="btn btn-warning btn-sm text-white" href="/biblio/livros?searchText={{ $a->nome }}" title="Buscar livros com este Autor"><i class="fas fa-search"></i></a>
               @elseif(Auth::guard('admin')->check())
                  <a class="btn btn-warning btn-sm text-white" href="/admin/livros?searchText={{ $a->nome }}" title="Buscar livros com este Autor"><i class="fas fa-search"></i></a>
                  @include('layouts.buttons-index',['routeedit'=>'AutoresController@edit', 'ids'=>$a->cpf])
                @else
                <a class="btn btn-warning btn-sm text-white" href="/livros?searchText={{ $a->nome }}" title="Buscar livros com este Autor"><i class="fas fa-search"></i></a>
               @endif
               </td>

            </tr>
               @endforeach
            </tbody>
         </table>

         @foreach($autor as $key=>$a)
          @include('layouts.modals.modal-delete',[
            'nametype'=>'Autor',
            'ids'=>$a->cpf,
            'namefield'=>$a->nome,
            'action'=>'AutoresController@destroy'
          ])
          @endforeach

      </div>

   </div>
   <!-- Paginação -->
   {{$autor->links('layouts.pagination')}}

@endsection
