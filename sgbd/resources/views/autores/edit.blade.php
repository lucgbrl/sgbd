@extends ('layouts.admin')
@section ('conteudo')

    <!-- Errors -->
    @include ('layouts.errors')     

    <div class="card">
        <div class="card-header">
            <!--Name Field -->
            <i class="fas fa-pencil-alt "></i>Editar Autor:  {{ $autor->nome }}
        </div>
        <div class="card-body">
            {!! Form::model($autor, ['method'=>'PATCH', 'route'=>['autores.update',$autor->cpf]]) !!}
            {{ Form::token() }}
            <div class='form-row'>
                {{--
                <div class="form-group col-sm-3">
                  <label for="cpf">CPF: </label>
                <input type="text" max="11" disabled value="{{$autor->cpf }}" min="11" name="cpf" id="cpf" class="form-control">
                </div>
                --}}
                <div class="form-group col-sm-5">
                  <label for="matricula">Nome Completo: </label>
                  <input type="text" name="nome"  value="{{$autor->nome }}" id="nome" class="form-control">
                </div>
                <div class="form-group col-sm-4">
                  <label for="nacionalidade">Nacionalidade: </label>
                  <input type="text" value="{{$autor->nacionalidade }}" name="nacionalidade" id="nacionalidade" class="form-control">
                </div>
              </div>
            <!--.. Aqui-->
            <!--buttons-->            
                @include ('layouts.update-buttom')            
            {!! Form::close() !!}
        </div>
    </div>

@endsection