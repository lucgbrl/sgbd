@extends ('layouts.admin')
@section ('conteudo')

    @if (count($errors)>0)
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Foram encontrados os seguintes erros: </strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <ul>
                @foreach ( $errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif



    <div class="card">
        <div class="card-header">
            <!--Name Field -->
            <i class="fas fa-pencil-alt "></i>Cadastrar Autor
        </div>
        <div class="card-body">
                        {!! Form::open(array('url'=>'/admin/autores','method'=>'POST', 'autocomplete'=>'off')) !!}
                        {{ Form::token() }}
                        <div class='form-row'>
                            {{--<div class="form-group col-sm-3">
                              <label for="cpf">CPF: </label>
                              <input type="text" max="11" min="11" name="cpf" id="cpf" class="form-control">
                            </div>--}}
                            <div class="form-group col-sm-5">
                              <label for="matricula">Nome Completo: </label>
                              <input type="text" name="nome" id="nome" class="form-control">
                            </div>
                            <div class="form-group col-sm-4">
                              <label for="nacionalidade">Nacionalidade: </label>
                              <input type="text" name="nacionalidade" id="nacionalidade" class="form-control">
                            </div>
                          </div>
                          <!--.. Aqui-->
                          <!--buttons-->
                          @include ('layouts.register-buttons')
                       {!! Form::close() !!}
        </div>
    </div>

@endsection