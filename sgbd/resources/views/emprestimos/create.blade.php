@extends ('layouts.admin')
@section ('conteudo')
@include('layouts.errors')

<div class="card">
    <div class="card-header">
        <!--Name Field -->
        <i class="fas fa-pencil-alt "></i>Realizar Empréstimo
    </div>
    <div class="card-body">
                @if(Auth::guard('admin')->check())
                    {!! Form::open(array('url'=>'/admin/emprestimos','method'=>'POST', 'autocomplete'=>'off')) !!}
                
                @elseif(Auth::guard('biblio')->check())
                    {!! Form::open(array('url'=>'/biblio/emprestimos','method'=>'POST', 'autocomplete'=>'off')) !!}
                @endif    
                    {{ Form::token() }}
                    <div class='form-row'>                        
                        <div class="form-group col-sm-5">
                            <label for="nome">Nome Completo: </label><br>
                            <select name="nome"  data-live-search="true" class="selectpicker"  id="nome">
                                <option selected disabled  value="">Escolha</option>
                                @foreach($user as $u)
                                  <option value="{{$u->cpf}}">{{$u->name}}</option>
                                @endforeach         
                            </select>
                        </div>                        
                        <div class="form-group col-sm-4">
                            <label for="livro">Livro: </label><br>
                            <select multiple name="livro[]"  data-live-search="true" class="selectpicker"  id="livro">
                                <option selected disabled  value="">Escolha</option>
                                @foreach($livros as $l)
                                  <option value="{{$l->ISBN}}">{{$l->titulo}}</option>
                                @endforeach         
                            </select>
                          </div>
                      </div>
                      <!--.. Aqui-->
                      <!--buttons-->
                      @include ('layouts.register-buttons')
                   {!! Form::close() !!}
    </div>
</div>

@endsection