<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{{asset('img/opt/icon.svg')}}" type="img/svg">

    <title>{{ config('app.name') }}</title> 
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">   
    <link href="css/style.css" rel="stylesheet">
  </head>
  <body class="home-page">

    <!-- Nav Bar Simplificada -->

    <nav class="navbar navbar-light bg-light static-top">
      <div class="container">
        <a class="navbar-brand" href="#">
            <img class="icon-color" width="9%" src="{{asset('img/opt/icon.svg')}}" title="Icon" class="d-inline-block" alt="">
            <p class="d-inline-block">
                {{ config('app.name', 'Sistema de Gerenciamento de Bibliotecas') }}</h1>
            <p>
        </a>          
        @if (Route::has('login'))
        <div class="top-right links">           
            @auth('admin')
                <a href="{{ url('/admin') }}">Painel</a>                          
            @endauth
            @auth('web')
            <a href="{{ url('/home') }}">Home</a>
            @endauth   
        </div>
        @endif
        @guest
        <div class="top-right links">
        <a href="{{ route('login') }}">Login</a>
        <a href="{{ route('register') }}">Register</a>   
        </div>
        @endguest
    
      </div>
    </nav>

    <!--
       Renomeei a classe Billboard para Masthead, segundo template no W3Schools
       Adicionando o Formato responsivo.
     -->

    <header class="masthead text-white text-center" style="background: url('{{asset('img/opt/home/bg-masthead.jpg')}}') no-repeat center center;">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <h1 class="mb-5">Sistema de Gerenciamento de Bibliotecas - SGBD </h1>
          </div>
          <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
            <form>
              <div class="form-row">
                <div class="col-12 col-md-9 mb-2 mb-md-0">
                  <input type="email" class="form-control form-control-lg" placeholder="Digite seu email...">
                </div>
                <div class="col-12 col-md-3">
                  <button type="submit" class="btn btn-block btn-lg btn-primary">Enviar</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </header>

    <!-- Lista de ícones em formato de grade  -->

    <section class="features-icons bg-light text-center">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
              <div class="features-icons-icon d-flex">
                <i class="icon-screen-desktop m-auto text-info"></i>
              </div>
              <h3>Ambiente Responsivo</h3>
              <p class="lead mb-0">Para dispositivos com todas as dimensões de tela</p>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
              <div class="features-icons-icon d-flex">
                <i class="icon-layers m-auto text-info"></i>
              </div>
              <h3>Escalável</h3>
              <p class="lead mb-0">A quantidade de usuários não é problema</p>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="features-icons-item mx-auto mb-0 mb-lg-3">
              <div class="features-icons-icon d-flex">
                <i class="icon-check m-auto text-info"></i>
              </div>
              <h3>Fácil de usar</h3>
              <p class="lead mb-0">Conta com uma interface descomplicada e um excelente guia de uso</p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- 
      
      Image Row
      Removido
    -->

    <!-- Image Showcases -->
    <section class="showcase">
      <div class="container-fluid p-0">
        <div class="row no-gutters">

        <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image: url('{{asset('img/opt/home/bg-showcase-2.jpg')}}');"></div>
          <div class="col-lg-6 order-lg-1 my-auto showcase-text">
            <h2>Desenvolvido utilizand as melhores práticas de mercado de Software</h2>
            <p class="lead mb-0">Bootstrap, Node JS, PHP e CSS3, utilizados de maneira heróica, pensado para grandes e pequenas telas!</p>
          </div>
        </div>
        <div class="row no-gutters">
          <div class="col-lg-6 text-white showcase-img" style="background-image: url('{{asset('img/opt/home/bg-showcase-1.jpg')}}');"></div>
          <div class="col-lg-6 my-auto showcase-text">
            <h2>Organização é a palavre de ordem</h2>
            <p class="lead mb-0">Fácil para o usuário final. Fácil para o administrador. Nosso foco sempre foi a praticidade para gerir o Acervo e as interações com Usuários.</p>
          </div>
        </div>
        <div class="row no-gutters">
          <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image: url('{{asset('img/opt/home/bg-showcase-3.jpg')}}');"></div>
          <div class="col-lg-6 order-lg-1 my-auto showcase-text">
            <h2>Fácil de usar</h2>
            <p class="lead mb-0">Nenhum truque ou ferramenta está oculta dos usuários. Sem necessidade de adquirir algum plugin adicional ou licenças caras.</p>
          </div>
        </div>
      </div>
    </section>

    <!-- Classe Testimonials -->
    <section class="testimonials text-center bg-light">
      <div class="container">
        <h2 class="mb-5">Veja o que as pessoas estão dizendo</h2>
        <div class="row">
          <div class="col-lg-4">
            <div class="testimonial-item mx-auto mb-5 mb-lg-0">
              <img class="img-fluid rounded-circle mb-3" src="{{asset('img/opt/home/t1.jpg')}}" alt="">
              <h5>Wesley Melo</h5>
              <p class="font-weight-light mb-0">"Melhor sistema do mercado atualmente nesse ramo!!"</p>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="testimonial-item mx-auto mb-5 mb-lg-0">
              <img class="img-fluid rounded-circle mb-3" src="{{asset('img/opt/home/t2.jpg')}}" alt="">
              <h5>Barbara Cavalcante</h5>
              <p class="font-weight-light mb-0">"Resolveu todos os problemas no aluguel de livros aqui na universidade!!"</p>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="testimonial-item mx-auto mb-5 mb-lg-0">
              <img class="img-fluid rounded-circle mb-3" src="{{asset('img/opt/home/t3.jpg')}}" alt="">
              <h5>Rafael José</h5>
              <p class="font-weight-light mb-0">"Muito bom, recomendo. É melhor do que cachaça!!"</p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Call to Action -->
    <section class="call-to-action text-white text-center" style="background: url('{{asset('img/opt/home/bg-masthead.jpg')}}') no-repeat center center;">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <h2 class="mb-4">Deseja experimentar? Envie um email para nós!</h2>
          </div>
          <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
            <form>
              <div class="form-row">
                <div class="col-12 col-md-9 mb-2 mb-md-0">
                  <input type="email" class="form-control form-control-lg" placeholder="Digite seu email...">
                </div>
                <div class="col-12 col-md-3">
                  <button type="submit" class="btn btn-block btn-lg btn-primary">Enviar</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>

    <!-- Footer -->
    <footer class="footer bg-light">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 h-100 text-center text-lg-left my-auto">
            <ul class="list-inline mb-2">              
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                  <a href="/sobre">Sobre</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="#">Política de Privacidade</a>
              </li>
              <li class="list-inline-item">
              <a href="{{route('admin.login')}}">Administrador</a>
                </li>
                <li class="list-inline-item">
                    <a href="{{route('biblio.login')}}">Bibliotecario</a>
                      </li>
            </ul>
          <p class="text-muted small mb-4 mb-lg-0">&copy; {{date('Y')}}- {{config('app.name')}}. Todos os direitos reservados. <img width="2%" class="" src="{{asset('/img/opt/juice.png')}}"></p>
          </div>
          <div class="col-lg-6 h-100 text-center text-lg-right my-auto">
            <ul class="list-inline mb-0">
              <li class="list-inline-item mr-3">
                <a href="#">
                  <i class="fab fa-facebook-f fa-2x fa-fw"></i>
                </a>
              </li>
              <li class="list-inline-item mr-3">
                <a href="#">
                  <i class="fab fa-twitter fa-2x fa-fw"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-instagram fa-2x fa-fw"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>

    <script type="text/javascript" src="{{asset('js/plugins.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/main.js')}}"></script>

  </body>

</html>
