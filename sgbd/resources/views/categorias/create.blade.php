@extends ('layouts.admin')
@section ('conteudo')

    @if (count($errors)>0)
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Foram encontrados os seguintes erros: </strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <ul>
                @foreach ( $errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif



    <div class="card">
        <div class="card-header">
            <!--Name Field -->
            <i class="fas fa-pencil-alt "></i>Cadastrar Categoria
        </div>
        <div class="card-body">
                        {!! Form::open(array('url'=>'/admin/categorias','method'=>'POST', 'autocomplete'=>'off')) !!}
                        {{ Form::token() }}
                        <div class='form-row'>
                            <div class="form-group col-sm-5">
                                <label for="categoria">Nome: </label>
                                <input type="text" name="nome_categoria" id="categoria" class="form-control">
                              </div>
                            <div class="form-group col-sm-5">
                                <label for="descrição">Descrição: </label>
                                <input type="text" name="descricao_categoria" id="descrição" class="form-control">
                            </div>                           
                          </div>
                          <!--.. Aqui-->
                          <!--buttons-->
                          @include ('layouts.register-buttons')
                       {!! Form::close() !!}
        </div>
    </div>

@endsection