@extends ('layouts.admin')
@section ('conteudo')

    <!-- Errors -->
    @include ('layouts.errors')    

    <div class="card">
        <div class="card-header">
            <!--Name Field -->
            <i class="fas fa-pencil-alt "></i>Editar Categoria:  {{ $categoria->nome }}
        </div>
        <div class="card-body">
            {!! Form::model($categoria, ['method'=>'PATCH', 'route'=>['categorias.update',$categoria->id_categoria]]) !!}
            {{ Form::token() }}
            <div class='form-row'>
                <div class="form-group col-sm-5">
                    <label for="categoria">Nome: </label>
                <input type="text" value="{{ $categoria->nome }}" name="nome_categoria" id="categoria" class="form-control">
                  </div>
                <div class="form-group col-sm-5">
                    <label for="descrição">Descrição: </label>
                    <input type="text" value="{{ $categoria->descricao }}" name="descricao_categoria" id="descrição" class="form-control">
                </div>   
              </div>
            <!--.. Aqui-->
            <!--buttons-->
            @include ('layouts.update-buttom')
            {!! Form::close() !!}
        </div>
    </div>

@endsection