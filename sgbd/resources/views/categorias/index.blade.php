@extends ('layouts.admin')
@section ('conteudo')

@include('layouts.errors')

   <div class="card">
    <div class="card-header">

      <!-- Search Form -->
      <div class="form-row">
        <div class="col-md-4 py-1">
          <!-- name field -->
          <i class="fas fa-list"></i>Categorias
        </div>
        <div class="col-md-4 ">
            @if(Auth::guard('admin')->check())
            <div class="text-center">
            <a href="/admin/categorias/create"><button class="btn btn-outline-color ">Incluir</button></a>
            </div>
            @endif
        </div>
        <div class="col-md-4">
          @if(Auth::guard('admin')->check())
            @include ('layouts.search',['search'=>'/admin/categorias'])
          @elseif(Auth::guard('biblio')->check())
          @include ('layouts.search',['search'=>'/biblio/categorias'])
          @else
          @include ('layouts.search',['search'=>'/categorias'])
          @endif
        </div>
      </div>

    </div>
    <div class="card-body wsc">
      <!-- Table-->
      <table class="table table-hover table-responsive">
        <thead>
          <tr>
            <!-- Modificar o cabeçalho de acordo com a tela-->
            <th>#</th>
            <th>Nome</th>
            <th>Descrição</th>
            <th>Ações</th>
          </tr>
        </thead>
        <tbody>
            <!-- Table Body - só basta colocar 1 campo como exemplo-->
            @foreach ($categoria as $key=>$ct)
            <tr>
               <td>{{ ++$key }}</td>
               <td>{{ $ct->nome }}</td>
               <td>{{ $ct->descricao }}</td>
               <td style='width:12%;'>
                  {{-- Buttons Edit and Delete--}}
                  @if(Auth::guard('biblio')->check())
                  <a class="btn btn-warning btn-sm text-white" href="/biblio/livros?searchText={{ $ct->nome }}" title="Buscar livros com este Autor"><i class="fas fa-search"></i></a>
                @elseif(Auth::guard('admin')->check())
                   <a class="btn btn-warning btn-sm text-white" href="/admin/livros?searchText={{ $ct->nome }}" title="Buscar livros com este Autor"><i class="fas fa-search"></i></a>
                  @include('layouts.buttons-index',['routeedit'=>'CategoriasController@edit', 'ids'=>$ct->id_categoria])
                  @else
                <a class="btn btn-warning btn-sm text-white" href="/livros?searchText={{ $ct->nome }}" title="Buscar livros com este Autor"><i class="fas fa-search"></i></a>
               @endif
               </td>
            </tr>
               @endforeach
            </tbody>

         </table>
         @foreach ($categoria as $key=>$ct)

            @include('layouts.modals.modal-delete',[
              'nametype'=>'Categorias',
              'ids'=>$ct->id_categoria,
              'namefield'=>$ct->nome,
              'action'=>'CategoriasController@destroy'
            ])
         @endforeach
      </div>

   </div>
   <!-- Paginação -->
   {{$categoria->links('layouts.pagination')}}

@endsection
