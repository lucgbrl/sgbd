@extends ('layouts.admin')
@section ('conteudo')

@include('layouts.errors')

   <div class="card">
      <div class="card-header">

         <!-- Search Form -->
         <div class="form-row">
            <div class="col-md-4 py-1">
               <!-- name field -->
               <i class="fas fa-list"></i>Cursos
            </div>
            <div class="col-md-4">
                @if(Auth::guard('admin')->check())
                <div class="text-center">
                <a href="/admin/cursos/create"><button class="btn btn-outline-color ">Incluir</button></a>
                </div>
                @endif
            </div>
            <div class="col-md-4">
                <!-- Insere campo de busca -->
                @include ('layouts.search',['search'=>'cursos'])
            </div>
         </div>

      </div>
      <div class="card-body wsc">
         <!-- Table-->
         <table class="table table-hover table-responsive">
            <thead>
            <tr>
               <!-- Modificar o cabeçalho de acordo com a tela-->
               <th>#</th>
               <th>Código do Curso</th>
               <th>Nome</th>
               <th>Ações</th>
            </tr>
            </thead>
            <tbody>
            <!-- Table Body - só basta colocar 1 campo como exemplo-->
            @foreach ($cursos as $key=>$c)
            <tr>
               <td>{{ ++$key }}</td>
               <td>{{ $c->cod_curso }}</td>
               <td>{{ $c->nome_curso }}</td>
               <td>
                    {{-- Buttons Edit and Delete--}}
                  @include('layouts.buttons-index',['routeedit'=>'CursosController@edit', 'ids'=>$c->cod_curso])  
               </td>
            </tr>
               @endforeach
            </tbody>
         </table>
        @foreach ($cursos as $key=>$c) 
            @include('layouts.modals.modal-delete',[
                'nametype'=>'Curso',
                'ids'=>$c->cod_curso,
                'namefield'=>$c->nome_curso,              
                'action'=>'CursosController@destroy'
            ])
        @endforeach 

      </div>

   </div>
   <!-- Paginação -->
   {{$cursos->links('layouts.pagination')}}

@endsection