@extends ('layouts.admin')
@section ('conteudo')

    @include('layouts.errors')
    
    <div class="card">
        <div class="card-header">
            <!--Name Field -->
            <i class="fas fa-pencil-alt "></i>Cadastrar Curso
        </div>
        <div class="card-body">
                        {!! Form::open(array('url'=>'/admin/cursos','method'=>'POST', 'autocomplete'=>'off')) !!}
                        {{ Form::token() }}
            <div class='form-row'>
                <div class="form-group col-sm-3">
                    <label for="codigo">Codigo do curso: </label>
                    <input type="text" name="cod_curso" id="codigo" class="form-control">
                </div>
                <div class="form-group col-sm-5">
                    <label for="curso">Nome: </label>
                    <input type="text" name="nome_curso" id="curso" class="form-control">
                </div>
            </div>
            <!--.. Aqui-->
            <!--buttons-->
            @include ('layouts.register-buttons')
                       {!! Form::close() !!}
        </div>
    </div>

@endsection