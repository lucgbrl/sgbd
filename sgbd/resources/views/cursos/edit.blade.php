@extends ('layouts.admin')
@section ('conteudo')

    <!-- Errors -->
    @include ('layouts.errors')
    <div class="card">
        <div class="card-header">
            <!--Name Field -->
            <i class="fas fa-pencil-alt "></i>Editar Curso:  {{ $cursos->cod_curso }}
        </div>
        <div class="card-body">
            {!! Form::model($cursos, ['method'=>'PATCH', 'route'=>['cursos.update',$cursos->cod_curso]]) !!}
            {{ Form::token() }}
            <div class='form-row'>
                <div class="form-group col-sm-3">
                    <label for="codigo">Codigo do curso: </label>
                    <input type="text" disabled name="cod_curso" value="{{ $cursos->cod_curso }}" id="codigo" class="form-control">
                </div>
                <div class="form-group col-sm-5">
                    <label for="curso">Nome: </label>
                    <input type="text" value="{{$cursos->nome_curso}}" name="nome_curso" id="curso" class="form-control">
                </div>
            </div>
            <!--.. Aqui-->
            <!--buttons-->
            @include ('layouts.update-buttom')        
            {!! Form::close() !!}
        </div>
    </div>

@endsection