@extends ('layouts.admin')
@section ('conteudo')

@include('layouts.errors')

   <div class="card">
    <div class="card-header">

      <!-- Search Form -->
      <div class="form-row">
        <div class="col-md-4 py-1">
          <!-- name field -->
          <i class="fas fa-list"></i>Administradores
        </div>
        <div class="col-md-4 "> 
          <div class="text-center">
          <a href="{{route('administradores.create')}}"><button class="btn btn-outline-color ">Incluir</button></a>
          </div>  
        </div>
        <div class="col-md-4">          
            @include ('layouts.search',['search'=>route('administradores.index')]) 
        </div>
      </div>

    </div>
    <div class="card-body wsc">
      <!-- Table-->
      <table class="table table-hover table-responsive">
        <thead>
          <tr>
            <!-- Modificar o cabeçalho de acordo com a tela-->
            <th>#</th>                        
            <th>Name</th>       
            <th>Email</th>
            <th>Username</th>                     
            <th>Ações</th>            
          </tr>
        </thead>
        <tbody>
            <!-- Table Body - só basta colocar 1 campo como exemplo-->
            @foreach ($administradores as $key=>$b)
            <tr>
               <td>{{ ++$key }}</td>              
               <td>{{ $b->name }}</td>              
               <td>{{ $b->email }}</td>
               <td>{{ $b->username }}</td>                             
               <td>
                 {{-- Buttons Edit and Delete--}}
                 @include('layouts.buttons-index',['routeedit'=>'AdministradoresController@edit', 'ids'=>$b->id])
               </td>               
            </tr>
               @endforeach
            </tbody>
            @foreach($administradores as $key=>$b)
            @include('layouts.modals.modal-delete',[
                'nametype'=>'Administrador',
                'ids'=>$b->id,
                'namefield'=>$b->name,              
                'action'=>'AdministradoresController@destroy'
            ])
            @endforeach
         </table>
      </div>

   </div>
   <!-- Paginação -->
   {{$administradores->links('layouts.pagination')}}

@endsection