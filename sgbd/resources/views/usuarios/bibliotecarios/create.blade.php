@extends ('layouts.admin')
@section ('conteudo')

    @include('layouts.errors')

    <div class="card">
        <div class="card-header">
            <!--Name Field -->
            <i class="fas fa-pencil-alt "></i>Cadastrar Bibliotecario
        </div>
        <div class="card-body">
                        {!! Form::open(array('url'=>route('bibliotecario.store'),'method'=>'POST', 'autocomplete'=>'off')) !!}
                        {{ Form::token() }}
                        <div class='form-row'>
                            <div class="form-group col-sm-3">
                              <label for="nome">Nome: </label>
                              <input type="text" name="nome" id="nome" class="form-control">
                            </div>
                            <div class="form-group col-sm-6">
                              <label for="email">Email: </label>
                              <input type="email" name="email" id="email" required class="form-control">
                            </div>                          
                            <div class="form-group col-sm-3">
                              <label for="username">Username: </label>
                              <input type="text" name="username" id="username" class="form-control">
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="username">Password: </label>
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                              </div>
                              <div class="form-group col-sm-3">
                                  <label for="username">Confirmar Password: </label>                                  
                                      <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                          <!--.. Aqui-->
                          <!--buttons-->
                        </div>
                          @include ('layouts.register-buttons')
                       {!! Form::close() !!}
        
    </div>
  </div>    
    

@endsection