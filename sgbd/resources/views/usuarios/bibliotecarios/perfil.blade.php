@extends ('layouts.admin')
@section ('conteudo')
@include('layouts.errors')

<div class="card">
  <div class="card-header">
      <!--Name Field -->
      <i class="fas fa-pencil-alt "></i>Editar Perfil:  {{ $user->name }}
  </div>
  <div class="card-body"> 
      {!! Form::model($user, ['method'=>'PATCH', 'route'=>
      ['editprofile.saveBiblio',$user->id]]) !!}   

      <div class="form-row">
          <div class="form-group col-sm-3">
          <label for="name" >{{ __('Nome Completo') }}</label>                            
              <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{$user->name}}" required>
          </div>   
         
          <div class="form-group col-sm-3">
              <label for="email" >{{ __('E-Mail Address') }}</label>
              
                  <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{$user->email}}" required>
          </div>
          <div class="form-group col-sm-3">
              <label for="username">{{ __('Username') }}</label>    
              
                  <input max="20" id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{$user->username}}" required>
          </div>
           
          <div class="form-group col-sm-3">
              <label for="password">{{ __('Password') }}</label>
      
                  <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password">
          </div>
          <div class="form-group col-sm-3">
              <label for="password">{{ __('Password') }}</label>
      
                  <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password">
          </div>
          <div class="form-group col-sm-3">
              <label for="password-confirm">{{ __('Confirm Password') }}</label>                                
                  <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
      
              </div>
          </div>



      @include ('layouts.update-buttom')
      {!! Form::close() !!}

      <script>
      $("input[name='name']").attr("disabled", "disabled");
      $("input[name='cpf']").attr("disabled", "disabled");
      $("input[name='curso']").attr("disabled", "disabled");
      $("input[name='siape']").attr("disabled", "disabled");
      </script>

  </div>
</div> 
@endsection