@extends ('layouts.admin')
@section ('conteudo')

@include('layouts.errors')

   <div class="card">
    <div class="card-header">

      <!-- Search Form -->
      <div class="form-row">
        <div class="col-md-4 py-1">
          <!-- name field -->
          <i class="fas fa-list"></i>Funcionários
        </div>
        <div class="col-md-4 ">
            <div class="text-center">
                @if(Auth::guard('admin')->check())
                  <a href="{{route('funcionariosadmin.create')}}"><button class="btn btn-outline-color ">Incluir</button></a>
                @elseif(Auth::guard('biblio')->check())
                <a href="{{route('funcionariosadmin.create')}}"><button class="btn btn-outline-color ">Incluir</button></a>
                @endif  
              </div>             
        </div>
        <div class="col-md-4">
            @if(Auth::guard('admin')->check())
            @include ('layouts.search',['search'=>route('funcionariosadmin.index')])
            @elseif(Auth::guard('biblio')->check())
            @include ('layouts.search',['search'=>route('funcionariosbiblio.index')])
            @endif
        </div>
      </div>

    </div>
    <div class="card-body wsc">
      <!-- Table-->
      <table class="table table-hover table-responsive">
        <thead>
          <tr>
            <!-- Modificar o cabeçalho de acordo com a tela-->
            <th>#</th>                        
            <th>Siape</th>       
            <th>Nome</th>
            <th>Username</th> 
            <th>Curso</th>           
            <th>Telefone</th>  
            <th>Endereço</th>                     
            <th>Ações</th>            
          </tr>
        </thead>
        <tbody>
            <!-- Table Body - só basta colocar 1 campo como exemplo-->
            @foreach ($funcionarios as $key=>$a)
            <tr>
               <td>{{ ++$key }}</td>              
               <td>{{ $a->Siape }}</td>              
               <td>{{ $a->Nome }}</td>
               <td>{{ $a->Usuario }}</td>
               <td>{{ $a->Curso }}</td>
               <td>{{ $a->Telefone }}</td>
               <td>{{ $a->Endereço }}</td> 
               <td style="width:12%">
                 {{-- Buttons Edit and Delete--}}
                 @if(Auth::guard('admin')->check())  
                 <a class="btn btn-info btn-sm text-white" href="{{ route('funcionariosadmin.details', ['id' => $a->id]) }}"><i class="fas fa-eye"></i></a>
                 @elseif(Auth::guard('biblio')->check())
                 <a class="btn btn-info btn-sm text-white" href="{{ route('funcionariosbiblio.details', ['id' => $a->id]) }}"><i class="fas fa-eye"></i></a>
                 @endif
  
                 @if(Auth::guard('admin')->check())
                 <a href="{{ route('funcionariosadmin.edit', ['id' => $a->id]) }}"><button class="btn btn-sm btn-color">
                    <i class="fas fa-edit"></i>
                 </button></a>
                 @elseif(Auth::guard('biblio')->check())
                 <a href="{{ route('funcionariosbiblio.edit', ['id' => $a->id]) }}"><button class="btn btn-sm btn-color">
                    <i class="fas fa-edit"></i>
                 </button></a>
  
                 @endif
                 @if(Auth::guard('admin')->check())
                 <a href="#"><button class="btn btn-sm btn-danger" data-toggle="modal" data-target="#{{'delete'.$a->id}}">
                    <i class="fas fa-trash"></i>
                 </button></a>
                 @endif
                 </td>               
              </tr>
                 @endforeach
              </tbody>
           </table>
           @if(Auth::guard('admin')->check())
           @foreach($funcionarios as $a)
              @include('layouts.modals.modal-delete',[
                  'nametype'=>'Bibliotecario:',
                  'ids'=>$a->id,
                  'namefield'=>$a->Nome,              
                  'action'=>'FuncionariosController@destroy'
              ])
              @endforeach
             @endif 
            </tbody>
         </table>
      </div>

   </div>
   <!-- Paginação -->
   {{$funcionarios->links('layouts.pagination')}}

@endsection