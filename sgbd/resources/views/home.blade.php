@extends('layouts.admin')
@section('conteudo')
<section class="content">
        <div class="container">
          @include('layouts.errors')
          <!-- alert-->
          @if (session('status'))
          <div class="alert alert-info alert-dismissible fade show" role="alert">
                {{ session('status') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
          <!-- page title-->
          <div class="card">
            <div class="card-header">
              Painel
            </div>
            <div class="card-body">
              {{--auth()->user()->tipo_usuario--}}
              {{--<p class="info">@component('layouts.who')@endcomponent</p>--}}
              @if(Auth::guard('web')->check())
              Olá, caro {{Auth::user()->tipo_usuario}} <strong> {{Auth::user()->name}} </strong>
              <p>Seja Bem vindo ao <strong> {{ config('app.name')}}.</strong> Este sistema foi feito para você consultar, livros, autores e seus empréstimos. </p>
              <p>Sempre verifique a data de devolução para que você não precise pagar multa.</p>
              @elseif(Auth::guard('biblio')->check())
              Olá, caro bibliotecário <strong> {{Auth::user()->name}}!! </strong>
              <p>Neste sistema você poderá locar livros para os alunos, professores e funcionários da Universiade Federal do Ceará</p>
              @elseif(Auth::guard('admin')->check())
              Olá, caro administrador <strong> {{Auth::user()->name}}!! </strong>
              <p>Aqui você terá acesso a todas as funções do sistema. Muito cuidado!! </p>


              @endif
    
            </div>
    
          </div>
          @if(Auth::guard('web')->check())
          
          <div class="card ">
            <div class="card-header">Emprestimos</div>
            <div class="card-body  wsc">
                <table class="table table-hover table-responsive">
                    <thead>                    
                      <tr>                        
                        <th>#</th> 
                        <th>Titulo</th>
                        <th>Data de Emprestimo</th>            
                        <th>Data de Devolução</th>            
                        <th>Recibo</th>            
                      </tr>
                    </thead>
                    
                    <tbody>   

                        @foreach ($emprestimouser as $key=>$e) 
                        <tr>
                           <td>{{ ++$key }}</td> 
                           <td>{{ $e->titulo_livro }}</td> 
                           <td>{{ $e->data_emprestimo }}</td> 
                           <td>{{ $e->data_devolucao }}</td>  
                           <td><a  href="{{ URL::action('EmprestimoController@generatepdf', $e->EN) }}" class="btn btn-color text-white"><i class="fas fa-file-pdf"></i></a> </td>
                        </tr>                          
                           @endforeach
                        </tbody>
                     </table> 
            </div>
          </div>
         
          @endif
          
        </div>    
      </section>
@endsection
