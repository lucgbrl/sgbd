<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Linhas de Autenticação
    |--------------------------------------------------------------------------
    |
    | As linhas a seguir são usadas durante a autenticação para várias 
    | mensagens que precisamos para mostrar pro usuário. Você é livre para 
    | modificar essas linhas de acordo com os requisitos da sua aplicação.
    |
    */

    'failed' => 'Essas credenciais não combinam com as dos nossos registros.',
    'throttle' => 'Muitas tentativas de login. Por favor tente em :seconds segundos.',

];
