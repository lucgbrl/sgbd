<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Linhas de linguagem para redefinição de senha
    |--------------------------------------------------------------------------
    |
    | As linhas a seguir são padronizadas e que combinam e que estão 
    | de acordo com as especificações exigidas pelo password broker numa tentativa 
    | falha de atualizar uma senha tais como um token invalido ou uma senha
    | inválida.
    |
    */

    'password' => 'As senhas devem conter no mínimo seis caracteres e combinar com a sua confirmação.',
    'reset' => 'Sua senha foi redefinida',
    'sent' => 'Enviamos para o seu e-mail o link para redefinição',
    'token' => 'Esse token para atualização de senha é inválido',
    'user' => "Não foi possível encontrar um usuário ligado à esse e-mail.",

];
