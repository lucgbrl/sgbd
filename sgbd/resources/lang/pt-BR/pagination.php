<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Linhas de Paginação.
    |--------------------------------------------------------------------------
    |
    | As linhasa seguir são usadsa pela biblioteca de paginação para construir 
    | links de paginação simples.
    | Fique à vontade para mudá-las para qualquer coisa que quiser
    | e customizar suas views para combinar melhor com a sua aplicação.
    |
    */

    'previous' => '&laquo; Anterior',
    'next' => 'Próxima &raquo;',

];
