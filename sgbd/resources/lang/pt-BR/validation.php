<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Linhas de linguagem para a validação
    |--------------------------------------------------------------------------
    |
    | As linhas de linguagem a seguir contem as mensgens de erro padrão usadas pela 
    | classe de validação. Algumas dessas tem múltiplas versões como as regras para 
    | tamanho. Fique à vontade para modificá-las.
    |
    */

    'accepted'             => 'O :attribute deve ser aceito.',
    'active_url'           => 'O :attribute não é uma URL valida.',
    'after'                => 'O :attribute deve ser uma data depois de :date.',
    'after_or_equal'       => 'O :attribute deve ser uma data posterior ou igual a :date.',
    'alpha'                => 'O :attribute pode conter somente letras.',
    'alpha_dash'           => 'O :attribute pode conter somente letras, números, e traços.',
    'alpha_num '            => 'O :attribute pode conter somente letras e números.',
    'array'                => 'O :attribute deve ser um array.',
    'before'               => 'O :attribute deve ser uma data antes de :date.',
    'before_or_equal'      => 'O :attribute deve ser uma data anterior ou igual a :date.',
    'between'              => [
        'numeric' => 'O :attribute deve estar entre :min e :max.',
        'file'    => 'O :attribute deve estar entre :min e :max kilobytes.',
        'string'  => 'O :attribute deve estar entre :min e :max caracteres.',
        'array'   => 'O :attribute deve ter entre :min e :max items.',
    ],
    'boolean'              => 'O campo :attribute deve ser verdadeiro ou falso.',
    'confirmed'            => 'A confirmação :attribute não combina.',
    'date'                 => 'O :attribute não é uma data válida.',
    'date_format'          => 'O :attribute não combina com o formato :format.',
    'different'            => 'O :attribute e :other devem ser diferentes.',
    'digits'               => 'O :attribute deve ser de :digits digitos.',
    'digits_between'       => 'O :attribute deve estar entre :min e :max digits.',
    'dimensions'           => 'O :attribute tem dimensões de imagens inválidas.',
    'distinct'             => 'O campo :attribute tem um valor duplicado.',
    'email'                => 'O :attribute deve ser um endereço de e-mail válido.',
    'exists'               => 'O :attribute selecionado é inválido.',
    'file'                 => 'O :attribute deve ser um arquivo.',
    'filled'               => 'O campo :attribute deve ter um valor.',
    'gt'                   => [
        'num eric' => 'O :attribute deve ser maior que :value.',
        'file'    => 'O :attribute deve ser maior que :value kilobytes.',
        'string'  => 'O :attribute deve ser maior que :value caracteres.',
        'array'   => 'O :attribute deve ter mais de :value items.',
    ],
    'gte'                  => [
        'num eric' => 'O :attribute deve ser maior ou igual a :value.',
        'file'    => 'O :attribute deve ser maior ou igual a :value kilobytes.',
        'string'  => 'O :attribute deve ser maior ou igual a :value caracteres.',
        'array'   => 'O :attribute deve conter :value items ou mais.',
    ],
    'image'                => 'O :attribute deve ser uma imagem.',
    'in'                   => 'O selected :attribute é inválido.',
    'in_array'             => 'O campo :attribute não existe em :other.',
    'integer'              => 'O :attribute deve ser um inteiro.',
    'ip'                   => 'O :attribute deve ser um endereço IP válido.',
    'ipv4'                 => 'O :attribute deve ser um endereço IPv4 válido.',
    'ipv6'                 => 'O :attribute deve ser um endereço IPv6 válido.',
    'json'                 => 'O :attribute deve ser uma string JSON válida.',
    'lt'                   => [
        'numeric' => 'O :attribute deve ser menor que :value.',
        'file'    => 'O :attribute deve ser menor que :value kilobytes.',
        'string'  => 'O :attribute deve ser menor que :value characters.',
        'array'   => 'O :attribute deve ser menor que :value items.',
    ],
    'lte'                  => [
        'numeric' => 'O :attribute deve ser menor ou igual a :value.',
        'file'    => 'O :attribute deve ser menor ou igual a :value kilobytes.',
        'string'  => 'O :attribute deve ser menor ou igual a :value characters.',
        'array'   => 'O :attribute não deve ter mais que :value items.',
    ],
    'max'                  => [
        'numeric' => 'O :attribute pode não ser maior que :max.',
        'file'    => 'O :attribute pode não ser maior que :max kilobytes.',
        'string'  => 'O :attribute pode não ser maior que :max characters.',
        'array'   => 'O :attribute pode não have more than :max items.',
    ],
    'mimes'                => 'O :attribute deve ser um arquivo do tipo: :values.',
    'mimetypes'            => 'O :attribute deve ser um arquivo do tipo: :values.',
    'min'                  => [
        'numeric' => 'O :attribute deve ser de no mínimo :min.',
        'file'    => 'O :attribute deve ser de no mínimo :min kilobytes.',
        'string'  => 'O :attribute deve ser de no mínimo :min characters.',
        'array'   => 'O :attribute deve ter no mínimo :min items.',
    ],
    'not_in'               => 'O :attribute selecionado é inválido.',
    'not_regex'            => 'O formato :attribute é inválido.',
    'numeric'              => 'O :attribute deve ser um número',
    'present'              => 'O campo :attribute deve estar presente.',
    'regex'                => 'O formato do campo :attribute é inválido.',
    'required'             => 'O campo :attribute é necessário.',
    'required_if'          => 'O campo :attribute é necessário quando :other é :value.',
    'required_unless'      => 'O campo :attribute é necessário a menos que :other esteja em :values.',
    'required_with'        => 'O campo :attribute é necessário quando :values está presente.',
    'required_with_all'    => 'O campo :attribute é necessário quando :values está presente.',
    'required_without'     => 'O campo :attribute é necessário quando :values não está presente.',
    'required_without_all' => 'O campo :attribute é necessário quando nenhum dos :values existem.',
    'same'                 => 'O :attribute e :other devem combinar.',
    'size'                 => [
        'numeric' => 'O :attribute deve ser :size.',
        'file'    => 'O :attribute deve ser :size kilobytes.',
        'string'  => 'O :attribute deve ser :size caracteres.',
        'array'   => 'O :attribute deve conter :size items.',
    ],
    'string'               => 'O :attribute deve ser uma string.',
    'timezone'             => 'O :attribute deve ser um zona válida.',
    'unique'               => 'O :attribute já existe.',
    'uploaded'             => 'O upload de :attribute falhou.',
    'url'                  => 'O formato :attribute é inválido.',

    /*
    |--------------------------------------------------------------------------
    | Linhas de customização da validação
    |--------------------------------------------------------------------------
    |
    | Aqui você pode especificamente customisar mensagens de validação para 
    | atributos usando a | convenção "attribute.rule" para nomear as linhas. Isso 
    | agiliza a especificação da customização de uma linha para dada regra de 
    | atributo.
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'mensagem customizada',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Customização dos atributos de validação 
    |--------------------------------------------------------------------------
    |
    | As linhas a seguir são usadas para trocar atributos comuns por algo mais
    | amigável ao leitor como um endereço de E-mail ao invés de 
    | apenas "email". Essa simplificação nos ajuda a criar mensagens mais enxutas.
    |
    */

    'attributes' => [],

];
