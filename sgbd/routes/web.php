<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/sobre', function(){
    return view('sobre');
});

//Route pesquisa cursos para registrar alunos,funcionarios e professores

Route::get('aluno-create', function(){
    $listcursos=DB::table('curso')->select('cod_curso','nome_curso')->get();
        return View::make('auth.aluno', ["cursos"=>$listcursos]);   
});
Route::get('professor-create', function(){  
        $listcursos=DB::table('curso')->select('cod_curso','nome_curso')->get();
        return View::make('auth.professor', ["cursos"=>$listcursos]);   
});

Route::get('funcionario-create', function(){
    $listcursos=DB::table('curso')->select('cod_curso','nome_curso')->get();
        return View::make('auth.funcionario', ["cursos"=>$listcursos]);   
});

//Ativa autenticação
Auth::routes();

//Route Users

Route::get('/livros', 'LivrosController@index')->name('livros.index')->middleware('auth:web');
Route::get('/autores', 'AutoresController@index')->name('autores.index')->middleware('auth:web');
Route::get('/home', 'HomeController@index')->name('home.dashboard');
Route::get('/users/logout', 'Auth\LoginController@userLogout')->name('user.logout');
Route::get('/categorias', 'CategoriasController@index')->name('categorias.index')->middleware('auth:web');

//Route Bibliotecario Auth
Route::prefix('biblio')->group(function(){
    Route::get('/login', 'Auth\BiblioLoginController@showLoginForm')->name('biblio.login');
    Route::post('/login', 'Auth\BiblioLoginController@login')->name('biblio.login.submit');
    Route::get('/', 'BiblioController@index')->name('biblio.dashboard');
    Route::get('/logout','Auth\BiblioLoginController@logout')->name('biblio.logout');
    //Recuperar senhas
    Route::post('/password/email', 'Auth\BiblioForgotPasswordController@sendResetLinkEmail')->name('biblio.password.email');
    Route::get('/password/reset', 'Auth\BiblioForgotPasswordController@showLinkRequestForm')->name('biblio.password.request');
    Route::post('/password/reset', 'Auth\BiblioResetPasswordController@reset');
    Route::get('/password/reset/{token}', 'Auth\BiblioResetPasswordController@showResetForm')->name('biblio.password.reset');
    //Livros
    Route::get('/livros', 'LivrosController@index')->name('livrosbiblio.index')->middleware('auth:biblio');
    //Autores
    Route::get('/autores', 'AutoresController@index')->name('autoresbiblio.index')->middleware('auth:biblio');
    //Categorias
    Route::get('/categorias', 'CategoriasController@index')->name('categorias.index')->middleware('auth:biblio');
    //Emprestimos
    Route::resource('emprestimos','EmprestimoController',['names'=>['destroy' => 'emprestimosbiblio.destroy']])->middleware('auth:biblio');
    //Alunos
    Route::resource('alunos','AlunosController',['names'=>[
        'index' => 'alunosbiblio.index',
        'create' => 'alunosbiblio.create',
        'edit' => 'alunosbiblio.edit',
        'update' =>'alunosbiblio.update',
        'store' => 'alunosbiblio.store',
        'show' => 'alunosbiblio.show',
        'destroy' => 'alunosbiblio.destroy',       
    ]])->middleware('auth:biblio');
    Route::get('/alunos/{aluno}/details','AlunosController@details')->name('alunosbiblio.details')->middleware('auth:biblio');
    //Professores
    Route::resource('professores','ProfessoresController',['names'=>[
        'index' => 'professoresbiblio.index',
        'create' => 'professoresbiblio.create',
        'edit' => 'professoresbiblio.edit',
        'update' =>'professoresbiblio.update',
        'store' => 'professoresbiblio.store',
        'show' => 'professoresbiblio.show',
        'destroy' => 'professoresbiblio.destroy',        
    ]])->middleware('auth:biblio');
    Route::get('/professores/{professor}/details','ProfessoresController@details')->name('professoresbiblio.details')->middleware('auth:biblio');
    //Funcionarios
    Route::resource('funcionarios','FuncionariosController',['names'=>[
        'index' => 'funcionariosbiblio.index',
        'create' => 'funcionariosbiblio.create',
        'edit' => 'funcionariosbiblio.edit',
        'update' =>'funcionariosbiblio.update',
        'store' => 'funcionariosbiblio.store',
        'show' => 'funcionariosbiblio.show',
        'destroy' => 'funcionariosbiblio.destroy',        
    ]])->middleware('auth:biblio');
    Route::get('/funcionarios/{funcionario}/details','FuncionariosController@details')->name('funcionariosbiblio.details')->middleware('auth:biblio');

    //Editar Perfil
    Route::get('/edit/{biblio}/profile','EditCadUserController@editBiblio')->name('editprofile.editBiblio')->middleware('auth:biblio');
    Route::patch('/edit/{biblio}','EditCadUserController@saveBiblio')->name('editprofile.saveBiblio')->middleware('auth:biblio');

});

//Routes Admin Auth
Route::prefix('admin')->group(function(){
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::get('/logout','Auth\AdminLoginController@logout')->name('admin.logout');
    //Recuperar senhas
    Route::post('/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('/password/reset', 'Auth\AdminResetPasswordController@reset');
    Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');
    //Rota Livros  
    Route::get('/livros', 'LivrosController@index')->name('livrosadmin.index')->middleware('auth:admin');
    Route::post('/livros', 'LivrosController@store')->name('livros.store')->middleware('auth:admin');
    Route::get('/livros/create','LivrosController@create')->name('livros.create')->middleware('auth:admin');
    Route::delete('/livros/{livro}','LivrosController@destroy')->name('livros.destroy')->middleware('auth:admin');
    Route::patch('/livros/{livro}','LivrosController@update')->name('livros.update')->middleware('auth:admin');
    Route::get('/livros/{livro}','LivrosController@show')->name('livros.show');    
    Route::get('/livros/{livro}/edit','LivrosController@edit')->name('livros.edit')->middleware('auth:admin');
    //Rota Autores
    Route::get('/autores', 'AutoresController@index')->name('autores.index')->middleware('auth:admin');
    Route::post('/autores', 'AutoresController@store')->name('autores.store')->middleware('auth:admin');
    Route::get('/autores/create','AutoresController@create')->name('autores.create')->middleware('auth:admin');
    Route::patch('/autores/{autore}','AutoresController@update')->name('autores.update')->middleware('auth:admin');
    Route::get('/autores/{autore}','AutoresController@show')->name('autores.show')->middleware('auth:admin');
    Route::delete('/autores/{autore}','AutoresController@destroy')->name('autores.destroy')->middleware('auth:admin');
    Route::get('/autores/{autore}/edit','AutoresController@edit')->name('autores.edit')->middleware('auth:admin');
    //Rota Cursos
    Route::get('/cursos', 'CursosController@index')->name('cursos.index')->middleware('auth:admin');
    Route::post('/cursos', 'CursosController@store')->name('cursos.store')->middleware('auth:admin');
    Route::get('/cursos/create','CursosController@create')->name('cursos.create')->middleware('auth:admin');
    Route::patch('/cursos/{cursos}','CursosController@update')->name('cursos.update')->middleware('auth:admin');
    Route::get('/cursos/{cursos}','CursosController@show')->name('cursos.show')->middleware('auth:admin');
    Route::delete('/cursos/{cursos}','CursosController@destroy')->name('cursos.destroy')->middleware('auth:admin');
    Route::get('/cursos/{cursos}/edit','CursosController@edit')->name('cursos.edit')->middleware('auth:admin');
    //Rota Categorias
    Route::get('/categorias', 'CategoriasController@index')->name('categorias.index')->middleware('auth:admin');
    Route::post('/categorias', 'CategoriasController@store')->name('categorias.store')->middleware('auth:admin');
    Route::get('/categorias/create','CategoriasController@create')->name('categorias.create')->middleware('auth:admin');
    Route::patch('/categorias/{categoria}','CategoriasController@update')->name('categorias.update')->middleware('auth:admin');
    Route::get('/categorias/{categoria}','CategoriasController@show')->name('categorias.show')->middleware('auth:admin');
    Route::delete('/categorias/{categoria}','CategoriasController@destroy')->name('categorias.destroy')->middleware('auth:admin');
    Route::get('/categorias/{categoria}/edit','CategoriasController@edit')->name('categorias.edit')->middleware('auth:admin');
    //Alunos
    Route::resource('alunos','AlunosController',['names'=>[
        'index' => 'alunosadmin.index',
        'create' => 'alunosadmin.create',
        'edit' => 'alunosadmin.edit',
        'update' =>'alunosadmin.update',
        'store' => 'alunosadmin.store',
        'show' => 'alunosadmin.show',
        'destroy' => 'alunosadmin.destroy',        
    ]])->middleware('auth:admin');
    Route::get('/alunos/{aluno}/details','AlunosController@details')->name('alunosadmin.details')->middleware('auth:admin');
   
    //Professores
    Route::resource('professores','ProfessoresController',['names'=>[
        'index' => 'professoresadmin.index',
        'create' => 'professoresadmin.create',
        'edit' => 'professoresadmin.edit',
        'update' =>'professoresadmin.update',
        'store' => 'professoresadmin.store',
        'show' => 'professoresadmin.show',
        'destroy' => 'professoresadmin.destroy',        
    ]])->middleware('auth:admin');
    Route::get('/professores/{professor}/details','ProfessoresController@details')->name('professoresadmin.details')->middleware('auth:admin');
    //Funcionarios
    Route::resource('funcionarios','FuncionariosController',['names'=>[
        'index' => 'funcionariosadmin.index',
        'create' => 'funcionariosadmin.create',
        'edit' => 'funcionariosadmin.edit',
        'update' =>'funcionariosadmin.update',
        'store' => 'funcionariosadmin.store',
        'show' => 'funcionariosadmin.show',
        'destroy' => 'funcionariosadmin.destroy',        
    ]])->middleware('auth:admin');
    Route::get('/funcionarios/{funcionario}/details','FuncionariosController@details')->name('funcionariosadmin.details')->middleware('auth:admin');
    //Emprestimo    
    Route::resource('emprestimos','EmprestimoController', ['names'=>['destroy' => 'emprestimosadmin.destroy']])->middleware('auth:admin');
    //bibliotecario
    Route::get('/bibliotecarios','BibliotecarioController@index')->name('bibliotecarios.index')->middleware('auth:admin');
    Route::get('/bibliotecarios/create','BibliotecarioController@create')->name('bibliotecarios.create')->middleware('auth:admin');
    Route::post('/bibliotecarios', 'BibliotecarioController@store')->name('bibliotecario.store')->middleware('auth:admin');
    Route::get('/bibliotecarios/{bibliotecarios}','BibliotecarioController@show')->name('bibliotecarios.show')->middleware('auth:admin');
    Route::get('/bibliotecarios/{bibliotecarios}/edit','BibliotecarioController@edit')->name('bibliotecarios.edit')->middleware('auth:admin');
    Route::delete('/bibliotecarios/{bibliotecarios}','BibliotecarioController@destroy')->name('bibliotecarios.destroy')->middleware('auth:admin');
    Route::patch('/bibliotecarios/{bibliotecarios}','BibliotecarioController@update')->name('bibliotecarios.update')->middleware('auth:admin');
    //Administradores
    Route::get('/administradores','AdministradoresController@index')->name('administradores.index')->middleware('auth:admin');
    Route::get('/administradores/create','AdministradoresController@create')->name('administradores.create')->middleware('auth:admin');
    Route::post('/administradores', 'AdministradoresController@store')->name('administradores.store')->middleware('auth:admin');
    Route::get('/administradores/{administradores}','AdministradoresController@show')->name('administradores.show')->middleware('auth:admin');
    Route::get('/administradores/{administradores}/edit','AdministradoresController@edit')->name('administradores.edit')->middleware('auth:admin');
    Route::delete('/administradores/{administradores}','AdministradoresController@destroy')->name('administradores.destroy')->middleware('auth:admin');
    Route::patch('/administradores/{administradores}','AdministradoresController@update')->name('administradores.update')->middleware('auth:admin');

});

//Emprestimo Gerar Recibo
Route::get('/emprestimos/recibo/{emprestimo}','EmprestimoController@generatepdf')->name('emprestimos.recibo');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//Edite Profile Aluno
Route::get('/edit/6o4sDQ5YzG89HQJvggHa/{aluno}/profile/','EditCadUserController@editAluno')->name('editprofile.editAluno')->middleware('auth:web');
Route::patch('/edit/6o4sDQ5YzG89HQJvggHa/{aluno}','EditCadUserController@saveAluno')->name('editprofile.saveAluno')->middleware('auth:web');
//Edit Profile Professor
Route::get('/edit/Z80SQTOlQRapUuc2dwzN/{professor}/profile','EditCadUserController@editProfessor')->name('editprofile.editProfessor')->middleware('auth:web');
Route::patch('/edit/Z80SQTOlQRapUuc2dwzN/{professor}','EditCadUserController@saveProfessor')->name('editprofile.saveProfessor')->middleware('auth:web');
//Edit Profile Funcionario
Route::get('/edit/m6mOMxQZVCE0UQVSgfwI/{funcionario}/profile','EditCadUserController@editFuncionario')->name('editprofile.editFuncionario')->middleware('auth:web');
Route::patch('/edit/m6mOMxQZVCE0UQVSgfwI/{funcionario}','EditCadUserController@saveFuncionario')->name('editprofile.saveFuncionario')->middleware('auth:web');
