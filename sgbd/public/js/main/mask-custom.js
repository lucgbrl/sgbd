//CPF
$('#cpf').mask('00000000000', {reverse: true});
//CEP
$('#cep').mask('000000-000', {reverse: true});
//Matricula
$('.matricula').mask('000000');
//Siape
$('.siape').mask('00000');

//Phone
var SPMaskBehavior = function (val) {
  return val.replace(/\D/g, '').length === 11 ? '(00)00000-0000' : '(00)0000-00009';
},
spOptions = {
  onKeyPress: function(val, e, field, options) {
      field.mask(SPMaskBehavior.apply({}, arguments), options);
    }
};
$('#telefone').mask(SPMaskBehavior, spOptions);

//ISBN
$('#ISBN').mask('000-0000000000');

//Date
$('.data').mask('00/00/0000');
//Year
$('#lançamento').mask('0000');
//Money
var MoneyOpts = {
  reverse:true,
  maxlength: false,
  placeholder: '0.00',
  onKeyPress: function(v, ev, curField, opts) {
    var mask = curField.data('mask').mask;
        decimalSep = (/0(.)00/gi).exec(mask)[1] || '.';
    if (curField.data('mask-isZero') && curField.data('mask-keycode') == 8)
      $(curField).val('');
    else if (v) {
      // remove previously added stuff at start of string
      v = v.replace(new RegExp('^0*\\'+decimalSep+'?0*'), ''); //v = v.replace(/^0*,?0*/, '');
      v = v.length == 0 ? '0'+decimalSep+'00' : (v.length == 1 ? '0'+decimalSep+'0'+v : (v.length == 2 ? '0'+decimalSep+v : v));
      $(curField).val(v).data('mask-isZero', (v=='0'+decimalSep+'00'));
    }
  }
};
$('.money').mask("#.##0,00", MoneyOpts);

