$(function () {
    $('#datetimepicker').datetimepicker({
        viewMode: 'years',
        format: 'DD-MM-YYYY',
        locale: 'pt',             
    });
  });