<?php

namespace sgbd;

use Illuminate\Database\Eloquent\Model;

class Cursos extends Model
{   public $incrementing = false;
    protected $table='curso';
    protected $primaryKey="cod_curso"; 

    public $timestamps=false;

    protected $fillable=[
        "cod_curso",
        "nome_curso"        
    ];

    protected $guarded=[

    ];

}
