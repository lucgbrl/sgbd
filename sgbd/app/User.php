<?php

namespace sgbd;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'username','password','endereço','cep','cpf','telefone','cidade','estado','tipo_usuario','bairro'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function aluno()
    {
        return $this->hasOne(Aluno::class,'id_user');
    }
    public function professor()
    {
        return $this->hasOne(Professor::class,'id_user');
    }
    public function funcionario()
    {
        return $this->hasOne(Funcionarios::class,'id_user');
    }


}
