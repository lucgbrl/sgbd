<?php

namespace sgbd;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use sgbd\Notifications\BiblioResetPasswordNotification;

class Biblio extends Authenticatable
{
    use Notifiable;
    protected $guard = 'biblio';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'username','password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    //Send password reset
    public function sendPasswordResetNotification($token){
        $this->notify(new BiblioResetPasswordNotification($token));
    }
}
