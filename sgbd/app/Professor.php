<?php

namespace sgbd;

use Illuminate\Database\Eloquent\Model;

class Professor extends Model
{
    protected $table='professor';
    protected $primaryKey='matricula_professor';     

    public $timestamps=false;

    protected $fillable=[
        'matricula_professor',
        'curso_cod_curso',
        'regime_trabalho',
        'id_user', 
    ];
}
