<?php

namespace sgbd;

use Illuminate\Database\Eloquent\Model;

class Funcionarios extends Model
{
    protected $table='funcionario';
    protected $primaryKey='matricula_funcionario';     

    public $timestamps=false;

    protected $fillable=[
        'matricula_funcionario',
        'cod_curso',        
        'id_user', 
    ];
}
