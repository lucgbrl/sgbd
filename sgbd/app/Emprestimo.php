<?php

namespace sgbd;

use Illuminate\Database\Eloquent\Model;

class Emprestimo extends Model
{
    protected $table='emprestimo';
    protected $primaryKey='id'; 

    public $timestamps=false;

    protected $fillable=[        
        'data_emprestimo',        
        'id_categoria',
        'id_user',
        'ISBN',   
    ];

    protected $guarded=[

    ];
}
