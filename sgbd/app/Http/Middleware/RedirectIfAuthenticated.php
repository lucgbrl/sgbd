<?php

namespace sgbd\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {   
        //change redirect login
        switch($guard){
            case 'admin':
                if(Auth::guard($guard)->check()):
                    return redirect()->route('admin.dashboard');
                endif;
            break;
            case 'biblio':
                if(Auth::guard($guard)->check()):
                    return redirect()->route('biblio.dashboard');
                endif;
            break;
            default:
                if(Auth::guard($guard)->check()):
                    return redirect('/home');
                endif;            
            break;
        } 
        
        /*if (Auth::guard($guard)->check()) {
            return redirect('/home');
        }*/

        return $next($request);
    }
}
