<?php

namespace sgbd\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LivrosFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //if($input['_method']='POST'):
            if($this->method() == 'POST'):
            return [
                'ISBN' => 'required|unique:livro|min:13|max:14|string',
                'titulo' => 'required',
                'lancamento' => 'required',
                'editora'=>'required',
                'categorias'=>'required',
                'copias'=>'required|min:1',
                'autores'=>'required'
                //'disponiveis'=>'required|min:1'
            ];

        //elseif($input['_method']='PATCH'):
            elseif($this->method() == 'PATCH'):
            return [                
                'titulo' => 'required',
                'lancamento' => 'required',
                'editora'=>'required',
                'categorias'=>'required',
                'copias'=>'required|min:1',
                'autores'=>'required'
            ];

        endif;    
    }
}
