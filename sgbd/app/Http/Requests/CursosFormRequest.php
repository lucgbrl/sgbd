<?php

namespace sgbd\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CursosFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
          //if($input['_method']='POST'):
        if($this->method() == 'POST'):
            return [
                'cod_curso' => 'required|unique:curso|max:3|min:3',
                'nome_curso' => 'required'
                //'disponiveis'=>'required|min:1'
            ];

        //elseif($input['_method']='PATCH'):
        elseif($this->method() == 'PATCH'):
            return [                
                'nome_curso' => 'required'
            ];
        else:
            return [                
                'nome_curso' => 'required'
            ];  

        endif;
    }
}