<?php

namespace sgbd\Http\Controllers;

use Illuminate\Http\Request;
use sgbd\Biblio;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\Rule;

class BibliotecarioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(Request $request){
        if ($request){
            $query=trim($request->get('searchText'));
            $bibliotecarios=Biblio::select('id','name','email','username')
            ->where('name','LIKE','%'.$query.'%')
            ->orderBy('name','asc')
            ->orwhere('email','LIKE','%'.$query.'%')
            ->orderBy('name','asc')  
            ->orwhere('username','LIKE','%'.$query.'%')
            ->orderBy('name','asc')             
            ->paginate(10);
            return view('usuarios.bibliotecarios.index',["bibliotecarios"=>$bibliotecarios,"searchText"=>$query ]);
        }
    }
    public function create(){
        return view('usuarios.bibliotecarios.create');
    }
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'nome' => 'required|string',
            'email' => 'required|email|unique:biblios',
            'username' => 'required|string|unique:biblios',
            'password' => 'required|string|min:6|confirmed',
        ]);  
        if ($validator->fails()) {           
            return redirect()
                        ->route('bibliotecarios.create')
                        ->withErrors($validator)
                        ->withInput();                     
        }
       try{
            $bibliotecario=new Biblio;
            $bibliotecario->name = $request['nome'];
            $bibliotecario->username = $request['username'];
            $bibliotecario->email = $request['email'];
            $bibliotecario->password = Hash::make($request['password']);
            $bibliotecario->save();
            return Redirect::to('/admin/bibliotecarios')
            ->with('success',"Bibliotecario adicionado com Sucesso!!!");
        }
        catch(\Exception $e){
            return redirect('/admin/bibliotecarios/create')
            ->withErrors('Erro!! - Tente novamente mais tarde')
            ->withInput();
        }  

    }
    public function show($id){
        return view("usuarios.bibliotecarios.show",["bibliotecario"=>Biblio::findOrFail($id)]);
    }
    public function edit($id){
        return view("usuarios.bibliotecarios.edit",["bibliotecario"=>Biblio::findOrFail($id)]);      
    }
    public function destroy($id){
        $bibliotecario=Biblio::findOrFail($id);
        $bibliotecario->delete();      
        return Redirect::to('/admin/bibliotecarios')
        ->with('success','Bibliotecario deletado com sucesso!!');
    }
    public function update(Request $request,$id){
        $validator = Validator::make($request->all(), [
            'nome' => 'required|string',
            'email' => 'required|'.Rule::unique('biblios')->ignore($id),
            'username' => 'required|'.Rule::unique('biblios')->ignore($id),  
        ]);  
        if ($validator->fails()) {           
            return redirect('admin/bibliotecarios/'.$id.'/edit')   
                        ->withErrors($validator)
                        ->withInput();                     
        }
        try{
            $bibliotecario=Biblio::findOrFail($id);
            $bibliotecario->name = $request->get('nome');
            $bibliotecario->email = $request->get('email');
            $bibliotecario->username = $request->get('username');
            if($request->get('password') != ''){
                $validator = Validator::make($request->all(), [
                    'password' => 'required|string|min:6|confirmed',
                ]);
                if ($validator->fails()) {           
                    return redirect('admin/bibliotecarios/'.$id.'/edit')   
                                ->withErrors($validator)
                                ->withInput();                     
                }
            $bibliotecario->password = Hash::make($request->get('password'));
            $bibliotecario->update(); 
            return Redirect::to('/admin/bibliotecarios')->with('success','Bibliotecario atualizado com sucesso');   
            }
            $bibliotecario->update();
            return Redirect::to('/admin/bibliotecarios')->with('success','Bibliotecario atualizado com sucesso');

        }catch(\Exception $e){
            return redirect('admin/bibliotecarios/'.$id.'/edit')            
            ->withErrors('Erro!! - Tente novamente mais tarde')
            ->withInput();
        }

    }

}
