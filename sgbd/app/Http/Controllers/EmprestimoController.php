<?php

namespace sgbd\Http\Controllers;

use Illuminate\Http\Request;
use sgbd\Http\Requests;
use sgbd\Emprestimo;
use sgbd\Livros;
use sgbd\User;
use Illuminate\Support\Facades\Redirect;
use sgbd\Http\Requests\AutoresFormRequest;
use Illuminate\Support\Facades\Validator;
use DB;
use Auth;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Crypt;

class EmprestimoController extends Controller
{
    public function index(Request $request){

        if ($request){
            $query=trim($request->get('searchText'));
            $emprestimos=DB::table('emprestimos')
            ->where('Nome','LIKE','%'.$query.'%')
            ->orderBy('data_devolucao','desc')
            ->orwhere('titulo_livro','LIKE','%'.$query.'%')
            ->orderBy('data_devolucao','desc')     
            ->orwhere('username','LIKE','%'.$query.'%')
            ->orderBy('data_devolucao','desc')
            ->paginate(10);        
            $empcript=clone $emprestimos;
            foreach($empcript as $e):
                $e->data_emprestimo=date('d-m-Y',strtotime( $e->data_emprestimo));
                $e->data_devolucao=date('d-m-Y',strtotime( $e->data_devolucao));
                $e->EN = Crypt::encrypt($e->NE);
            endforeach;    
            return view('emprestimos.index',["emprestimo"=>$empcript,"searchText"=>$query ]);
        }    

    }
      //view create
      public function  create(){ 
        $livros=Livros::select('ISBN','titulo')->where('disponiveis','<>', '0')->get(); 
        $user=User::select('cpf','name','tipo_usuario')->orderby('name','asc')->get();
        return view("emprestimos.create",['livros'=>$livros,'user'=>$user]);
    }
      //store
      public function store(Request $request ){

        $validator = Validator::make($request->all(), [
            'livro' => 'required',
            'nome' => 'required',
        ]);  
        if ($validator->fails()) {
            if(Auth::guard('admin')->check()):
            return redirect('/admin/emprestimos/create')
                        ->withErrors($validator)
                        ->withInput();
            elseif(Auth::guard('biblio')->check()):
            return redirect('/biblio/emprestimos/create')
            ->withErrors($validator)
            ->withInput();            
            endif;            
        }

         try{
            $typeuser=User::select('id')->where('cpf', $request['nome'])->first()->id;            
            $livros=array_unique($request['livro']);
            foreach ($livros as $l):
            $emprestimo=new Emprestimo;
            $emprestimo->ISBN=$l;
            $emprestimo->id_user=$typeuser; 
            $emprestimo->save();
            endforeach;  
            if(Auth::guard('admin')->check()):          
            return Redirect::to('/admin/emprestimos')->with('success',"Emprestimo Realizado com Sucesso");
            elseif(Auth::guard('biblio')->check()):          
            return Redirect::to('/biblio/emprestimos')->with('success',"Emprestimo Realizado com Sucesso");
            endif;

         }catch(\Exception $e){
            if(Auth::guard('admin')->check()):  
            return redirect('/admin/emprestimos/create')
            ->withErrors('O Usuário já atingiu o número maximo de emprestimos.')
            ->withInput();
            elseif(Auth::guard('biblio')->check()):  
                return redirect('/biblio/emprestimos/create')
                ->withErrors('O Usuário já atingiu o número maximo de emprestimos.')
                ->withInput();
            endif;
         }
      }
      public function destroy($id){
        $emprestimo=Emprestimo::findOrFail($id);
        $emprestimo->delete();
        if(Auth::guard('admin')->check()):
        return Redirect::to('/admin/emprestimos')->with('success','Livro devolvido com Sucesso!!');
        elseif(Auth::guard('biblio')->check()):
            return Redirect::to('/biblio/emprestimos')->with('success','Livro devolvido com Sucesso!!');
        endif;
      }  
      //gerar pdf
      public function generatepdf($id){  
        $decript = Crypt::decrypt($id);   
        $emprestimos = DB::table('emprestimos')->where('NE',$decript)->first(); 
        $pdf = PDF::loadView('pdf.pdf-emprestimo', ['data'=>$emprestimos]);
        return $pdf->setPaper([0, 0, 300.874, 221.102], 'landscape')->download('recibo.pdf');
       }
   
}
