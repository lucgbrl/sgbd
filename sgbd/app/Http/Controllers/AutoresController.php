<?php

namespace sgbd\Http\Controllers;

use Illuminate\Http\Request;
use sgbd\Http\Requests;
use sgbd\Autores;
use Illuminate\Support\Facades\Redirect;
use sgbd\Http\Requests\AutoresFormRequest;
use DB;
use Auth;

class AutoresController extends Controller
{
    public function __construct(){


    }
    //search and find table
    public function index(Request $request){

        if ($request){
            $query=trim($request->get('searchText'));
            $autor=DB::table('autor')
            ->where('nome','LIKE','%'.$query.'%')
            ->orderBy('nome','asc')
            ->orwhere('nacionalidade','LIKE','%'.$query.'%')
            ->orderBy('nome','asc')              
            ->paginate(10);
            return view('autores.index',["autor"=>$autor,"searchText"=>$query ]);
        }
    }
    //view create
    public function  create(){
        return view("autores.create");
    }
    //save to database
    public function store( AutoresFormRequest $request){        
        $autor=new Autores;
        //$autor->cpf=trim(mb_strtoupper($request->get('cpf'),'UTF-8'));
        $autor->nome=trim(mb_strtoupper($request->get('nome'),'UTF-8'));
        $autor->nacionalidade=trim(mb_strtoupper($request->get('nacionalidade'),'UTF-8'));
        $autor->save();
        if(Auth::guard('admin')->check()):  
        return Redirect::to('/admin/autores')->with('success','Autor adicionado com sucesso');
        else: endif;
    }
    //show
    public function show($id){
        return view("autores.show",["autor"=>Autores::findOrFail($id)]);
    }
    //edit
    public function edit($id){
        return view("autores.edit",["autor"=>Autores::findOrFail($id)]);
    }
    //update
    public function update(AutoresFormRequest $request,$id){
        $autor=Autores::findOrFail($id);              
        $autor->nome=trim(mb_strtoupper($request->get('nome'),'UTF-8'));
        $autor->nacionalidade=trim(mb_strtoupper($request->get('nacionalidade'),'UTF-8'));       
        $autor->update();
        if(Auth::guard('admin')->check()):  
        return Redirect::to('/admin/autores')->with('success','Autor atualizado com sucesso');
        else: endif;
    }
        //Delete
    public function destroy($id){
        $autor=Autores::findOrFail($id);
        $autor->delete();
        return Redirect::to('autores')->with('success','Autor deletado com sucesso');
    }
}
