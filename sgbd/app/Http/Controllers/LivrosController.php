<?php

namespace sgbd\Http\Controllers;
use Illuminate\Http\Request;
use sgbd\Http\Requests;
use sgbd\Livros;
use sgbd\Autores;
use Illuminate\Support\Facades\Redirect;
use sgbd\Http\Requests\LivrosFormRequest;
use DB;
use Auth;

class LivrosController extends Controller
{
    public function __construct(){


    }
    public function index(Request $request){

        if ($request){
            $query=trim($request->get('searchText'));
            $livros=DB::table('livros')
            ->where('ISBN','LIKE','%'.$query.'%')
            ->orderBy('titulo','asc')
            ->orwhere('titulo','LIKE','%'.$query.'%')
            ->orderBy('titulo','asc')
            ->orwhere('ano_lancamento','LIKE','%'.$query.'%')
            ->orderBy('titulo','asc')
            ->orwhere('editora','LIKE','%'.$query.'%')
            ->orderBy('titulo','asc')
            ->orwhere('total','LIKE','%'.$query.'%')
            ->orderBy('titulo','asc')
            ->orwhere('disponiveis','LIKE','%'.$query.'%')
            ->orderBy('titulo','asc')
            ->orwhere('categoria','LIKE','%'.$query.'%')
            ->orderBy('titulo','asc')
            ->orwhere('autor','LIKE','%'.$query.'%')
            ->orderBy('titulo','asc')
            ->paginate(10);
            return view('livros.index',["livros"=>$livros,"searchText"=>$query ]);
        }
    }
    //create books
    public function  create(){
        //Faz a pesquisa das categorias e autores e envia para view
        $categorias=DB::table('categoria')->get();
        $autores=DB::table('autor')->get();
        return view("livros.create",["categoria"=>$categorias, "autores"=>$autores]);
    }

    public function store( LivrosFormRequest $request){
        //ele armazena na banco de dados
        $livros=new Livros;
        $autores_ids=array_unique($request['autores']);
        $ISBN=trim(mb_strtoupper($request->get('ISBN'),'UTF-8'));
        $livros->ISBN=trim(mb_strtoupper($request->get('ISBN'),'UTF-8'));
        $livros->titulo=trim(mb_strtoupper($request->get('titulo'),'UTF-8'));
        $livros->ano_lancamento=trim(mb_strtoupper($request->get('lancamento'),'UTF-8'));
        $livros->editora=trim(mb_strtoupper($request->get('editora'),'UTF-8'));
        $livros->quantidade_copias=$request->get('copias');
        $livros->id_categoria=trim(mb_strtoupper($request->get('categorias'),'UTF-8'));
        //$livros->disponiveis=default;
        $livros->save();
        //Espaço destinado a salvar os autores.
        foreach ($autores_ids as $autor):
            DB::table('livro_has_autores')->insert([
                'livro_ISBN'=>$ISBN,
                'autores_CPF'=>$autor
            ]);
            endforeach;
            if(Auth::guard('admin')->check()):
        return Redirect::to('/admin/livros')->with('success','Livro adicionado com sucesso');
            else:

            endif;
}
//show
public function show($id){
    return view("livros.show",["categoria"=>Livros::findOrFail($id)]);
}
//edit
public function edit($id){
    $categorias=DB::table('categoria')->get();
    $livro_w_autores=DB::select( DB::raw(
        "SELECT DISTINCT nome, cpf FROM autor WHERE cpf IN (SELECT autores_cpf          FROM livro_has_autores WHERE livro_ISBN='$id')") );
    $autor_wo_livro=DB::select( DB::raw(
    "SELECT DISTINCT nome, cpf FROM autor WHERE cpf NOT IN (SELECT autores_cpf FROM livro_has_autores WHERE livro_ISBN='$id')") );
    return view("livros.edit",["livro"=>Livros::findOrFail($id), "categoria"=>$categorias,'outros'=>$autor_wo_livro,'autoreslivros'=>$livro_w_autores]);
}

//update
public function update(LivrosFormRequest $request,$id){
  try{
    $livros=Livros::findOrFail($id);
    $livros->titulo=trim(mb_strtoupper($request->get('titulo'),'UTF-8'));
    $livros->ano_lancamento=trim(mb_strtoupper($request->get('lancamento'),'UTF-8'));
    $livros->editora=trim(mb_strtoupper($request->get('editora'),'UTF-8'));
    $livros->quantidade_copias=$request->get('copias');
    $livros->id_categoria=trim(mb_strtoupper($request->get('categorias'),'UTF-8'));
    $livros->update();
    //remove autores antigos e adiciona os novos.
    $autores_ids=array_unique($request['autores']);
    //primeiro deleta os autores do livro
    DB::table('livro_has_autores')->where('livro_ISBN','=',$id)->delete();
    //insert new autors
    foreach ($autores_ids as $autor):
        DB::table('livro_has_autores')->insert([
            'livro_ISBN'=>$id,
            'autores_CPF'=>$autor
        ]);
    endforeach;
    return Redirect::route('livrosadmin.index')->with('success','Livro atualizado com sucesso');
  }catch(\Exception $e){
    return Redirect::route('livrosadmin.index')->withErrors('Não foi possível atualizar o número de copias! Número de emprestimos é maior que a quantidade de cópias!!!');
  }
}
//delete
public function destroy($id){
    $autor=Livros::findOrFail($id);
    $autor->delete();
    return Redirect::route('livrosadmin.index')->with('success','Livro deletado com sucesso');
}
}
