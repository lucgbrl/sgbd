<?php

namespace sgbd\Http\Controllers;

use Illuminate\Http\Request;
use sgbd\Admin;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\Rule;

class AdministradoresController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(Request $request){
        if ($request){
            $query=trim($request->get('searchText'));
            $administradores=Admin::select('id','name','email','username')
            ->where('name','LIKE','%'.$query.'%')
            ->orderBy('name','asc')
            ->orwhere('email','LIKE','%'.$query.'%')
            ->orderBy('name','asc')  
            ->orwhere('username','LIKE','%'.$query.'%')
            ->orderBy('name','asc')             
            ->paginate(10);
            return view('usuarios.administradores.index',["administradores"=>$administradores,"searchText"=>$query ]);
        }
    }
    public function create(){
        return view('usuarios.administradores.create');
    }
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'nome' => 'required|string',
            'email' => 'required|email|unique:admins',
            'username' => 'required|string|unique:admins',
            'password' => 'required|string|min:6|confirmed',
        ]);  
        if ($validator->fails()) {           
            return redirect()
                        ->route('administradores.create')
                        ->withErrors($validator)
                        ->withInput();                     
        }
       try{
            $administradores=new Admin;
            $administradores->name = $request['nome'];
            $administradores->username = $request['username'];
            $administradores->email = $request['email'];
            $administradores->password = Hash::make($request['password']);
            $administradores->save();
            return Redirect::to('/admin/administradores')
            ->with('success',"Administrador adicionado com Sucesso!!!");
        }
        catch(\Exception $e){
            return redirect('/admin/administradores/create')
            ->withErrors('Erro!! - Tente novamente mais tarde')
            ->withInput();
        }  

    }
    public function show($id){
        return view("usuarios.administradores.show",["administradores"=>Admin::findOrFail($id)]);
    }
    public function edit($id){
        return view("usuarios.administradores.edit",["administradores"=>Admin::findOrFail($id)]);      
    }
    public function destroy($id){
        $administradores=Admin::findOrFail($id);
        $administradores->delete();      
        return Redirect::to('/admin/administradores')
        ->with('success','Administrador deletado com sucesso!!');
    }
    public function update(Request $request,$id){
        $validator = Validator::make($request->all(), [
            'nome' => 'required|string',
            'email' => 'required|'.Rule::unique('admins')->ignore($id),
            'username' => 'required|'.Rule::unique('admins')->ignore($id),  
        ]);  
        if ($validator->fails()) {           
            return redirect('admin/administradores/'.$id.'/edit')   
                        ->withErrors($validator)
                        ->withInput();                     
        }
        try{
            $administradores=Admin::findOrFail($id);
            $administradores->name = $request->get('nome');
            $administradores->email = $request->get('email');
            $administradores->username = $request->get('username');
            if($request->get('password') != ''){
                $validator = Validator::make($request->all(), [
                    'password' => 'required|string|min:6|confirmed',
                ]);
                if ($validator->fails()) {           
                    return redirect('admin/administradores/'.$id.'/edit')   
                                ->withErrors($validator)
                                ->withInput();                     
                }
            $administradores->password = Hash::make($request->get('password'));
            $administradores->update(); 
            return Redirect::to('/admin/administradores')->with('success','Administrador atualizado com sucesso');   
            }
            $administradores->update();
            return Redirect::to('/admin/administradores')->with('success','Administrador atualizado com sucesso');

        }catch(\Exception $e){
            return redirect('admin/administradores/'.$id.'/edit')            
            ->withErrors('Erro!! - Tente novamente mais tarde')
            ->withInput();
        }

    }
}
