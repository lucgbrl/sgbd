<?php

namespace sgbd\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use sgbd\Http\Requests;
use DB;
use Auth;
use sgbd\Cursos;
use sgbd\User;
use sgbd\Aluno;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class AlunosController extends Controller
{
    //Funções Index, store, update, edit, delete
    public function index(Request $request){
        if ($request){
            $query=trim($request->get('searchText'));
            $alunos=DB::table('aluno_curso')
                ->where('Nome','LIKE','%'.$query.'%')
                ->orderBy('Nome','asc')
                ->orwhere('Curso','LIKE','%'.$query.'%')
                ->orderBy('Nome','asc')
                ->orwhere('Matricula','LIKE','%'.$query.'%')
                ->orderBy('Nome','asc')
                ->paginate(10);
                $alunoc=clone $alunos;
                foreach ($alunos as $aluno):
                    $aluno->id=Aluno::select('id_user')->where('matricula_aluno',$aluno->Matricula)->value('id_user');
                    $aluno->DataConclusao=date('d/m/Y',strtotime($aluno->DataConclusao));
                endforeach;
            return view('usuarios.alunos.index',["alunos"=>$alunoc,"searchText"=>$query ]);
        }

    }
    public function create(){
        $cursos = Cursos::select('cod_curso','nome_curso')->get();
        return view('usuarios.alunos.create',['cursos'=>$cursos]);
    }
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'cpf' => 'required|string|unique:users|max:11|min:11',
            'cep'=>'required|string',
            'logradouro'=>'required|string',
            'bairro'=>'required|string',
            'estado'=>'required|string',
            'cidade'=>'required|string',
            'telefone'=>'required|string|min:10|max:14',
            'username' => 'required|string|max:20|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'regime' => 'string',
            'curso' => 'required|string',
            'matricula_aluno' => 'required|unique:aluno',
        ]);
        if ($validator->fails()) {
            if(Auth::guard('biblio')->check()):
            return redirect()
                        ->route('alunoscreatebiblio.create')
                        ->withErrors($validator)
                        ->withInput();
            elseif(Auth::guard('admin')->check()):
                return redirect()
                        ->route('alunosadmin.create')
                        ->withErrors($validator)
                        ->withInput();
        endif;
        }
        //Store
        try{
            $user=new User;
            $user->name = $request['name'];
            $user->cpf = $request['cpf'];
            $user->endereço = $request['logradouro'];
            $user->cep = $request['cep'];
            $user->bairro = $request['bairro'];
            $user->cidade = $request['cidade'];
            $user->estado = $request['estado'];
            $user->telefone = $request['telefone'];
            $user->username = $request['username'];
            $user->tipo_usuario = 'aluno';
            $user->email = $request['email'];
            $user->password = Hash::make($request['password']);
            $user->save();
            $aluno=new Aluno;
            $userId = $user->id;
            $aluno->matricula_aluno = $request['matricula_aluno'];
            $aluno->curso_cod_curso = $request['curso'];
            $aluno->data_ingreso = date('Y-m-d H:i:s',strtotime($request['data']));
            $aluno->id_user = $userId;
            $aluno->save();
            if(Auth::guard('biblio')->check()):
            return Redirect::to('/biblio/alunos')
            ->with('success',"Aluno adicionado com Sucesso!!!");
            elseif(Auth::guard('admin')->check()):
                return Redirect::to('/admin/alunos')
                ->with('success',"Aluno adicionado com Sucesso!!!");
            endif;
        }
        catch(\Exception $e){
            if(Auth::guard('biblio')->check()):
            return redirect('/biblio/alunos/create')
            ->withErrors('Erro!! - Tente novamente mais tarde')
            ->withInput();
            elseif(Auth::guard('admin')->check()):
                return redirect('/admin/alunos/create')
                ->withErrors('Erro!! - Tente novamente mais tarde')
                ->withInput();
            endif;
        }
    }
    public function show($id){
        return view("usuarios.alunos.show",["user"=>User::findOrFail($id)]);
    }

    public function edit($id){
        $user = User::findOrFail($id);
        $aluno = Aluno::select('matricula_aluno','data_ingreso','curso_cod_curso')->where('id_user',$id)->first();
        $buscacod = Aluno::select('curso_cod_curso')->where('id_user',$id)->value('curso_cod_curso');
        $curso = Cursos::select('cod_curso','nome_curso')->where('cod_curso',$buscacod)->first();
        $allcurso = Cursos::select('cod_curso','nome_curso')->where('cod_curso','<>',$buscacod)->get();
        return view("usuarios.alunos.edit",["user"=>User::findOrFail($id),'aluno'=>$aluno, 'cursoal' => $curso,'cursos'=>$allcurso ]);
    }
    public function update(Request $request, $id){

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'cpf' => 'required|string|max:11|min:11|'.Rule::unique('users')->ignore($id),
            'cep'=>'required|string',
            'logradouro'=>'required|string',
            'bairro'=>'required|string',
            'estado'=>'required|string',
            'cidade'=>'required|string',
            'telefone'=>'required|string|min:10|max:14',
            'username' => 'required|string|max:20|'.Rule::unique('users')->ignore($id),
            'email' => 'required|string|email|max:255|'.Rule::unique('users')->ignore($id),
           // 'password' => 'required|string|min:6|confirmed',
            'regime' => 'string',
            'curso' => 'required|string',
        ]);
        if ($validator->fails()) {
            if(Auth::guard('biblio')->check()):
            return redirect('biblio/alunos/'.$id.'/edit')
                        ->withErrors($validator)
                        ->withInput();
            elseif(Auth::guard('admin')->check()):
                return redirect('admin/alunos/'.$id.'/edit')
                        ->withErrors($validator)
                        ->withInput();
        endif;
        }
        try{
        $user = User::findOrFail($id);
        $search = Aluno::select('matricula_aluno')
        ->where('id_user',$id)->value('matricula_aluno');
        $aluno = Aluno::findOrFail($search);
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->cpf = $request->get('cpf');
        $user->endereço = $request->get('logradouro');        $user->cep = $request->get('cep');
        $user->bairro = $request->get('bairro');
        $user->cidade = $request->get('cidade');
        $user->estado = $request->get('estado');
        $user->telefone = $request->get('telefone');
        $user->username = $request->get('username');
        $user->update();
        $aluno->matricula_aluno = $request->get('matricula');
        $aluno->curso_cod_curso = $request->get('curso');
        $aluno->data_ingreso = date('Y-m-d H:i:s',strtotime($request['data']));
        $aluno->update();
        if($request->get('password') != ''){
            $validator = Validator::make($request->all(), [
                'password' => 'required|string|min:6|confirmed',
            ]);
            if ($validator->fails()) {
                return redirect('admin/alunos/'.$id.'/edit')
                            ->withErrors($validator)
                            ->withInput();
            }
        $user->password = Hash::make($request['password']);
        $user->update();
        if(Auth::guard('admin')->check()):
        return Redirect::to('/admin/alunos')->with('success','Aluno atualizado com sucesso');
        elseif(Auth::guard('biblio')->check()):
        return Redirect::to('/biblio/alunos')->with('success','Aluno atualizado com sucesso');
        endif;
        }
        if(Auth::guard('admin')->check()){
            return Redirect::to('/admin/alunos')->with('success','Aluno atualizado com sucesso');
        }
            elseif(Auth::guard('biblio')->check()){

            return Redirect::to('/biblio/alunos')->with('success','Aluno atualizado com sucesso');
        }

     }catch(\Exception $e){
        if(Auth::guard('admin')->check()){
        return redirect('admin/alunos/'.$id.'/edit')
        ->withErrors('Erro!! - Tente novamente mais tarde')
        ->withInput();
        }elseif(Auth::guard('biblio')->check()){
        return redirect('biblio/alunos/'.$id.'/edit')
        ->withErrors('Erro!! - Tente novamente mais tarde')
        ->withInput();
        }
     }

    }
    public function destroy($id){
        if(Auth::guard('admin')->check()){
         try{
            $user=User::findOrFail($id);
            $user->delete();
            return Redirect::to('/admin/alunos')
            ->with('success','Aluno deletado com sucesso!!');
        }catch(\Exception $e){
            return Redirect::to('/admin/alunos')
            ->withErrors('Erro!! - Tente novamente mais tarde')
            ->withInput();
        }

        }else{
            return Redirect::to('/biblio/alunos')
            ->withErrors('Erro!! - Você não tem permissão de deletar cadastros');
        }
    }
    //Detalhes
    public function details($id){
        $user = User::findOrFail($id);
        $aluno = Aluno::select('matricula_aluno','data_ingreso','curso_cod_curso')->where('id_user',$id)->first();
        $buscacod = Aluno::select('curso_cod_curso')->where('id_user',$id)->value('curso_cod_curso');
        $curso = Cursos::select('cod_curso','nome_curso')->where('cod_curso',$buscacod)->first();
        $allcurso = Cursos::select('cod_curso','nome_curso')->where('cod_curso','<>',$buscacod)->get();
        return view("usuarios.alunos.details",["user"=>User::findOrFail($id),'aluno'=>$aluno, 'cursoal' => $curso,'cursos'=>$allcurso ]);
    }
}
