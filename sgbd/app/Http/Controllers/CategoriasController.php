<?php

namespace sgbd\Http\Controllers;

use Illuminate\Http\Request;
use sgbd\Http\Requests;
use sgbd\Categorias;
use Illuminate\Support\Facades\Redirect;
use sgbd\Http\Requests\CategoriasFormRequest;
use DB;

class CategoriasController extends Controller
{
    public function __construct(){


    }
    //search and find table
    public function index(Request $request){

        if ($request){
            $query=trim($request->get('searchText'));
            $categoria=DB::table('categoria')
            ->where('nome','LIKE','%'.$query.'%')
            ->orderBy('nome','asc')
            ->orwhere('descricao','LIKE','%'.$query.'%')
            ->orderBy('nome','asc')              
            ->paginate(10);
            return view('categorias.index',["categoria"=>$categoria,"searchText"=>$query ]);
        }
    }

    //view create
    public function  create(){
        return view("categorias.create");
    }
    //save to database
    public function store( CategoriasFormRequest $request){        
        $categoria=new Categorias;
        $categoria->descricao=trim(mb_strtoupper($request->get('descricao_categoria'),'UTF-8'));
        $categoria->nome=trim(mb_strtoupper($request->get('nome_categoria'),'UTF-8'));
        $categoria->save();
        return Redirect::to('/admin/categorias')->with('success','Categoria adicionada com sucesso');
    }
    //show
    public function show($id){
        return view("categorias.show",["categoria"=>Categorias::findOrFail($id)]);
    }
    //edit
    public function edit($id){
        return view("categorias.edit",["categoria"=>Categorias::findOrFail($id)]);
    }
    //update
    public function update(CategoriasFormRequest $request,$id){
        $categoria=Categorias::findOrFail($id);              
        $categoria->descricao=trim(mb_strtoupper($request->get('descricao_categoria'),'UTF-8'));
        $categoria->nome=trim(mb_strtoupper($request->get('nome_categoria'),'UTF-8'));
        $categoria->update();
        return Redirect::to('/admin/categorias')->with('success','Categoria atualizada com sucesso');
    }
    //Delete
    public function destroy($id){
        $categoria=Categorias::findOrFail($id);
        $categoria->delete();
        return Redirect::to('/admin/categorias')->with('success','Categoria deletada com sucesso');
    }
}
