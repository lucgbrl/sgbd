<?php

namespace sgbd\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use sgbd\User;
use Illuminate\Support\Facades\Crypt;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
            //procura id pelo nome de usuario
            $username = User::select('username')
            ->where('id',Auth::user()->id)->value('username');
            $emprestimos=DB::table('emprestimos')
            ->where('username', $username)          
            ->orderBy('data_devolucao','desc')           
            ->paginate(10);        
            $empcrypt=clone $emprestimos;
            foreach($empcrypt as $e):
                $e->data_emprestimo=date('d-m-Y',strtotime( $e->data_emprestimo));
                $e->data_devolucao=date('d-m-Y',strtotime( $e->data_devolucao));
                $e->EN = Crypt::encrypt($e->NE);
            endforeach;    
            return view('home',["emprestimouser"=>$empcrypt]);
        
}
}
