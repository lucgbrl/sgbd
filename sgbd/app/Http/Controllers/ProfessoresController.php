<?php

namespace sgbd\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use sgbd\Http\Requests;
use DB;
use Auth;
use sgbd\Cursos;
use sgbd\User;
use sgbd\Professor;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class ProfessoresController extends Controller
{
    public function index(Request $request){
        if ($request){
            $query=trim($request->get('searchText'));
            $professores=DB::table('professores')
                ->where('Nome','LIKE','%'.$query.'%')
                ->orderBy('Nome','asc')
                ->orwhere('curso','LIKE','%'.$query.'%')
                ->orderBy('Nome','asc')
                ->orwhere('cpf','LIKE','%'.$query.'%')
                ->orderBy('Nome','asc')
                ->orwhere('Siape','LIKE','%'.$query.'%')
                ->orderBy('Nome','asc')
                ->paginate(10);  
                $professores=clone $professores;
                foreach ($professores as $professor):
                    $professor->id=Professor::select('id_user')->where('matricula_professor',$professor->Siape)->value('id_user');
                endforeach;              
            return view('usuarios.professores.index',["professores"=>$professores,"searchText"=>$query ]);
        }
    }
    public function create(){
        $cursos = Cursos::select('cod_curso','nome_curso')->get();
        return view('usuarios.professores.create',['cursos'=>$cursos]);
    }
    //Store
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'cpf' => 'required|string|unique:users|max:11|min:11', 
            'cep'=>'required|string',
            'logradouro'=>'required|string',
            'bairro'=>'required|string',
            'estado'=>'required|string',
            'cidade'=>'required|string',
            'telefone'=>'required|string|min:10|max:14',
            'username' => 'required|string|max:20|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'regime' => 'string',
            'curso' => 'required|string',
            'matricula_professor' => 'required|unique:professor',
            'regime' => 'required',
        ]);  
        if ($validator->fails()) {   
            if(Auth::guard('biblio')->check()):        
            return redirect()
                        ->route('professoresbiblio.create')
                        ->withErrors($validator)
                        ->withInput();
            elseif(Auth::guard('admin')->check()):
                return redirect()
                        ->route('professoresadmin.create')
                        ->withErrors($validator)
                        ->withInput();
        endif;
        }
        //Store
        try{
            $user=new User;           
            $user->name = $request['name'];
            $user->cpf = $request['cpf'];
            $user->endereço = $request['logradouro'];
            $user->cep = $request['cep'];
            $user->bairro = $request['bairro'];
            $user->cidade = $request['cidade'];
            $user->estado = $request['estado'];
            $user->telefone = $request['telefone'];
            $user->username = $request['username'];
            $user->tipo_usuario = 'professor';
            $user->email = $request['email'];
            $user->password = Hash::make($request['password']);
            $user->save();            
            $professor=new Professor;
            $userId = $user->id;
            $professor->matricula_professor = $request['matricula_professor'];
            $professor->curso_cod_curso = $request['curso'];
            $professor->regime_trabalho = $request['regime'];            
            $professor->id_user = $userId;
            $professor->save();
            if(Auth::guard('biblio')->check()): 
            return Redirect::to('/biblio/professores')
            ->with('success'," Professor adicionado com Sucesso!!!");
            elseif(Auth::guard('admin')->check()):
                return Redirect::to('/admin/professores')
                ->with('success',"Professor adicionado com Sucesso!!!");
            endif;     
        }
        catch(\Exception $e){
            if(Auth::guard('biblio')->check()): 
            return redirect('/biblio/professores/create')
            ->withErrors('Erro!! - Tente novamente mais tarde')
            ->withInput();
            elseif(Auth::guard('admin')->check()):
                return redirect('/admin/professores/create')
                ->withErrors('Erro!! - Tente novamente mais tarde')
                ->withInput();            
            endif;
        }  
    }
    //Show
    public function show($id){       
        return view("usuarios.professores.show",["user"=>User::findOrFail($id)]);
    }
    //Edit
    public function edit($id){
        $user = User::findOrFail($id);
        $professor = Professor::select('matricula_professor','regime_trabalho','curso_cod_curso')->where('id_user',$id)->first();
        $buscacod = Professor::select('curso_cod_curso')->where('id_user',$id)->value('curso_cod_curso');
        $curso = Cursos::select('cod_curso','nome_curso')->where('cod_curso',$buscacod)->first();
        $allcurso = Cursos::select('cod_curso','nome_curso')->where('cod_curso','<>',$buscacod)->get();
        return view("usuarios.professores.edit",["user"=>User::findOrFail($id),'professor'=>$professor, 'cursoal' => $curso,'cursos'=>$allcurso ]);
    } 
    //update
    public function update(Request $request, $id){
       
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'cpf' => 'required|string|max:11|min:11|'.Rule::unique('users')->ignore($id),
            'cep'=>'required|string',
            'logradouro'=>'required|string',
            'bairro'=>'required|string',
            'estado'=>'required|string',
            'cidade'=>'required|string',
            'telefone'=>'required|string|min:10|max:14',
            'username' => 'required|string|max:20|'.Rule::unique('users')->ignore($id),
            'email' => 'required|string|email|max:255|'.Rule::unique('users')->ignore($id),
           // 'password' => 'required|string|min:6|confirmed',
            'regime' => 'required|string',
            'curso' => 'required|string',
                   
        ]);  
        if ($validator->fails()) {   
            if(Auth::guard('biblio')->check()):        
            return redirect('biblio/professores/'.$id.'/edit')                        
                        ->withErrors($validator)
                        ->withInput();
            elseif(Auth::guard('admin')->check()):
                return redirect('admin/professores/'.$id.'/edit')
                        ->withErrors($validator)
                        ->withInput();
        endif;
        }
        try{
        $user = User::findOrFail($id);
        $search = Professor::select('matricula_professor')
        ->where('id_user',$id)->value('matricula_professor');
        $professor = Professor::findOrFail($search);
        $user->name = $request->get('name');
        $user->email = $request->get('email');        
        $user->cpf = $request->get('cpf');
        $user->endereço = $request->get('logradouro');        
        $user->cep = $request->get('cep');
        $user->bairro = $request->get('bairro');
        $user->cidade = $request->get('cidade');
        $user->estado = $request->get('estado');
        $user->telefone = $request->get('telefone');
        $user->username = $request->get('username');        
        $user->update();
        $professor->professor = $request->get('matricula_professor');
        $professor->curso_cod_curso = $request->get('curso');
        $professor->regime_trabalho = $request->get('regime');
        if($request->get('password') != ''){
            $validator = Validator::make($request->all(), [
                'password' => 'required|string|min:6|confirmed',
            ]);
            if ($validator->fails()) {           
                return redirect('admin/professores/'.$id.'/edit')   
                            ->withErrors($validator)
                            ->withInput();                     
            }
        $user->password = Hash::make($request['password']);
        $user->update();
        if(Auth::guard('admin')->check()):
        return Redirect::to('/admin/professores')->with('success','Professor atualizado com sucesso'); 
        elseif(Auth::guard('biblio')->check()):
        return Redirect::to('/biblio/professores')->with('success','Professor atualizado com sucesso');    
        endif;
        }
        if(Auth::guard('admin')->check()){
            return Redirect::to('/admin/professores')->with('success','Professor atualizado com sucesso'); 
        }
            elseif(Auth::guard('biblio')->check()){

            return Redirect::to('/biblio/professores')->with('success','Professor atualizado com sucesso');   
        } 
            
     }catch(\Exception $e){
        if(Auth::guard('admin')->check()){
        return redirect('admin/professores/'.$id.'/edit')            
        ->withErrors('Erro!! - Tente novamente mais tarde')
        ->withInput();
        }elseif(Auth::guard('biblio')->check()){
        return redirect('biblio/professores/'.$id.'/edit')            
        ->withErrors('Erro!! - Tente novamente mais tarde')
        ->withInput(); 
        }
     }

    }
    //Detalhes
    public function details($id){
        $user = User::findOrFail($id);
        $professor = Professor::select('matricula_professor','regime_trabalho','curso_cod_curso')->where('id_user',$id)->first();
        $buscacod = Professor::select('curso_cod_curso')->where('id_user',$id)->value('curso_cod_curso');
        $curso = Cursos::select('cod_curso','nome_curso')->where('cod_curso',$buscacod)->first();
        $allcurso = Cursos::select('cod_curso','nome_curso')->where('cod_curso','<>',$buscacod)->get();
        return view("usuarios.professores.details",["user"=>User::findOrFail($id),'professor'=>$professor, 'cursoal' => $curso,'cursos'=>$allcurso ]);
    } 
    //Delete
    public function destroy($id){
        if(Auth::guard('admin')->check()){
         try{
            $user=User::findOrFail($id);
            $user->delete();      
            return Redirect::to('/admin/professores')
            ->with('success','Professor deletado com sucesso!!');
        }catch(\Exception $e){
            return Redirect::to('/admin/professores')
            ->withErrors('Erro!! - Tente novamente mais tarde')
            ->withInput();
        }

        }else{
            return Redirect::to('/biblio/professores')
            ->withErrors('Erro!! - Você não tem permissão de deletar cadastros');
        }
    }

}