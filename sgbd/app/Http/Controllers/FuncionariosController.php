<?php

namespace sgbd\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use sgbd\Http\Requests;
use DB;
use Auth;
use sgbd\Cursos;
use sgbd\User;
use sgbd\Funcionarios;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class FuncionariosController extends Controller
{
    public function index(Request $request){
        if ($request){
            $query=trim($request->get('searchText'));
            $funcionarios=DB::table('funcionarios')
                ->where('Nome','LIKE','%'.$query.'%')
                ->orderBy('Nome','asc')
                ->orwhere('Curso','LIKE','%'.$query.'%')
                ->orderBy('Nome','asc')
                ->orwhere('cpf','LIKE','%'.$query.'%')
                ->orderBy('Nome','asc')
                ->orwhere('Siape','LIKE','%'.$query.'%')
                ->orderBy('Nome','asc')
                ->paginate(10);            
                $funcionarios=clone $funcionarios;
                foreach ($funcionarios as $funcionario):
                    $funcionario->id=Funcionarios::select('id_user')->where('matricula_funcionario',$funcionario->Siape)->value('id_user');
                endforeach;      
            return view('usuarios.funcionarios.index',["funcionarios"=>$funcionarios,"searchText"=>$query ]);
        }
    }
    //Create
    public function create(){
        $cursos = Cursos::select('cod_curso','nome_curso')->get();
        return view('usuarios.funcionarios.create',['cursos'=>$cursos]);
    }
    //Store
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'cpf' => 'required|string|unique:users|max:11|min:11', 
            'cep'=>'required|string',
            'logradouro'=>'required|string',
            'bairro'=>'required|string',
            'estado'=>'required|string',
            'cidade'=>'required|string',
            'telefone'=>'required|string|min:10|max:14',
            'username' => 'required|string|max:20|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',            
            'curso' => 'required|string',
            'matricula_funcionario' => 'required|unique:funcionario',
           
        ]);  
        if ($validator->fails()) {   
            if(Auth::guard('biblio')->check()):        
            return redirect()
                        ->route('funcionariosbiblio.create')
                        ->withErrors($validator)
                        ->withInput();
            elseif(Auth::guard('admin')->check()):
                return redirect()
                        ->route('funcionariosadmin.create')
                        ->withErrors($validator)
                        ->withInput();
        endif;
        }
        //Store
        try{
            $user=new User;           
            $user->name = $request['name'];
            $user->cpf = $request['cpf'];
            $user->endereço = $request['logradouro'];
            $user->cep = $request['cep'];
            $user->bairro = $request['bairro'];
            $user->cidade = $request['cidade'];
            $user->estado = $request['estado'];
            $user->telefone = $request['telefone'];
            $user->username = $request['username'];
            $user->tipo_usuario = 'funcionario';
            $user->email = $request['email'];
            $user->password = Hash::make($request['password']);
            $user->save();            
            $funcionarios=new Funcionarios;
            $userId = $user->id;
            $funcionarios->matricula_funcionario = $request['matricula_funcionario'];
            $funcionarios->cod_curso = $request['curso']; 
            $funcionarios->id_user = $userId;
            $funcionarios->save();
            if(Auth::guard('biblio')->check()): 
            return Redirect::to('/biblio/funcionarios')
            ->with('success'," Funcionario adicionado com Sucesso!!!");
            elseif(Auth::guard('admin')->check()):
                return Redirect::to('/admin/funcionarios')
                ->with('success',"Funcionario adicionado com Sucesso!!!");
            endif;     
        }
        catch(\Exception $e){
            if(Auth::guard('biblio')->check()): 
            return redirect('/biblio/funcionarios/create')
            ->withErrors('Erro!! - Tente novamente mais tarde')
            ->withInput();
            elseif(Auth::guard('admin')->check()):
                return redirect('/admin/funcionarios/create')
                ->withErrors('Erro!! - Tente novamente mais tarde')
                ->withInput();            
            endif;
        }  
    }
    
     //Show
     public function show($id){       
        return view("usuarios.funcionarios.show",["user"=>User::findOrFail($id)]);
    }
    //Edit
    public function edit($id){
        $user = User::findOrFail($id);
        $funcionario = Funcionarios::select('matricula_funcionario','cod_curso')->where('id_user',$id)->first();
        $buscacod = Funcionarios::select('cod_curso')->where('id_user',$id)->value('cod_curso');
        $curso = Cursos::select('cod_curso','nome_curso')->where('cod_curso',$buscacod)->first();
        $allcurso = Cursos::select('cod_curso','nome_curso')->where('cod_curso','<>',$buscacod)->get();
        return view("usuarios.funcionarios.edit",["user"=>User::findOrFail($id),'funcionario'=>$funcionario, 'cursoal' => $curso,'cursos'=>$allcurso ]);
    }
    // update
    public function update(Request $request, $id){
       
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'cpf' => 'required|string|max:11|min:11|'.Rule::unique('users')->ignore($id),
            'cep'=>'required|string',
            'logradouro'=>'required|string',
            'bairro'=>'required|string',
            'estado'=>'required|string',
            'cidade'=>'required|string',
            'telefone'=>'required|string|min:10|max:14',
            'username' => 'required|string|max:20|'.Rule::unique('users')->ignore($id),
            'email' => 'required|string|email|max:255|'.Rule::unique('users')->ignore($id),
           // 'password' => 'required|string|min:6|confirmed',   
            'curso' => 'required|string',
                   
        ]);  
        if ($validator->fails()) {   
            if(Auth::guard('biblio')->check()):        
            return redirect('biblio/funcionarios/'.$id.'/edit')                        
                        ->withErrors($validator)
                        ->withInput();
            elseif(Auth::guard('admin')->check()):
                return redirect('admin/funcionarios/'.$id.'/edit')
                        ->withErrors($validator)
                        ->withInput();
        endif;
        }
        try{
        $user = User::findOrFail($id);
        //busca matricula, através do ID
        $search = Funcionarios::select('matricula_funcionario')
        ->where('id_user',$id)->value('matricula_funcionario');
        $funcionarios = Funcionarios::findOrFail($search);
        $user->name = $request->get('name');
        $user->email = $request->get('email');        
        $user->cpf = $request->get('cpf');
        $user->endereço = $request->get('logradouro');        
        $user->cep = $request->get('cep');
        $user->bairro = $request->get('bairro');
        $user->cidade = $request->get('cidade');
        $user->estado = $request->get('estado');
        $user->telefone = $request->get('telefone');
        $user->username = $request->get('username');        
        $user->update();
        $funcionarios->matricula_funcionario = $request->get('matricula_funcionario');
        $funcionarios->cod_curso = $request->get('curso');      
        if($request->get('password') != ''){
            $validator = Validator::make($request->all(), [
                'password' => 'required|string|min:6|confirmed',
            ]);
            if ($validator->fails()) {           
                return redirect('admin/funcionarios/'.$id.'/edit')   
                            ->withErrors($validator)
                            ->withInput();                     
            }
        $user->password = Hash::make($request['password']);
        $user->update();
        if(Auth::guard('admin')->check()):
        return Redirect::to('/admin/funcionarios')->with('success','Funcionario atualizado com sucesso'); 
        elseif(Auth::guard('biblio')->check()):
        return Redirect::to('/biblio/funcionarios')->with('success','Funcionario atualizado com sucesso');    
        endif;
        }
        if(Auth::guard('admin')->check()){
            return Redirect::to('/admin/funcionarios')->with('success','Funcionario atualizado com sucesso'); 
        }
            elseif(Auth::guard('biblio')->check()){

            return Redirect::to('/biblio/funcionarios')->with('success','Funcionario atualizado com sucesso');   
        } 
            
     }catch(\Exception $e){
        if(Auth::guard('admin')->check()){
        return redirect('admin/funcionarios/'.$id.'/edit')            
        ->withErrors('Erro!! - Tente novamente mais tarde')
        ->withInput();
        }elseif(Auth::guard('biblio')->check()){
        return redirect('biblio/funcionarios/'.$id.'/edit')            
        ->withErrors('Erro!! - Tente novamente mais tarde')
        ->withInput(); 
        }
     }

    }
    //details
    public function details($id){
        $user = User::findOrFail($id);
        $funcionario = Funcionarios::select('matricula_funcionario','cod_curso')->where('id_user',$id)->first();
        $buscacod = Funcionarios::select('cod_curso')->where('id_user',$id)->value('cod_curso');
        $curso = Cursos::select('cod_curso','nome_curso')->where('cod_curso',$buscacod)->first();        
        return view("usuarios.funcionarios.details",["user"=>User::findOrFail($id),'funcionario'=>$funcionario, 'cursoal' => $curso ]);
    }

    public function destroy($id){
        if(Auth::guard('admin')->check()){
         try{
            $user=User::findOrFail($id);
            $user->delete();      
            return Redirect::to('/admin/funcionarios')
            ->with('success','Funcionario deletado com sucesso!!');
        }catch(\Exception $e){
            return Redirect::to('/admin/funcionarios')
            ->withErrors('Erro!! - Tente novamente mais tarde')
            ->withInput();
        }

        }else{
            return Redirect::to('/biblio/funcionarios')
            ->withErrors('Erro!! - Você não tem permissão de deletar cadastros');
        }
    }

}
