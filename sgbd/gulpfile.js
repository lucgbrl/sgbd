//Adiciona os modulos instalados
//primeira coisa que se faz é chamar é o require do gulp
const gulp = require('gulp');
//gulp sass
const sass =require('gulp-sass');
//gulp autoprefixer
const autoprefixer =require('gulp-autoprefixer');
//browser sysnc serve para atualizar o navegador quando salvar
const browserSync = require('browser-sync').create();
//gulp concate serve para juntar os javascripts em um mesmo arquivo.
const concate = require('gulp-concat');
//gulp-babel transforma a sintaxe nova de js na antiga para suportar outros navegadores.
const babel = require('gulp-babel');
//gulp-uglify minimiza o js atenção: ele é antigo, mas como foi usado o gulp-babel entao ele funciona.
const uglify = require('gulp-uglify');
//gulp-imagemin otimiza todas as imagens colocadas no projeto gif, png, jpeg e svg
const imagemin = require('gulp-imagemin');

//funcao para compilar sass
function compilaSass(){
    //entra em todas as pastas do scss e todos os arquivos .scss scss/**/*.scss
    return gulp
        .src('public/css/scss/**/*.scss')
    //transforma em sass os arquivos
        .pipe(sass({/*compime css*/ outputStyle:'compressed'}))
        //adiciona automaticamente os prefixos para os browsers
        .pipe(autoprefixer({ browsers: ['last 2 versions'], cascade: false }))
    //destino do arquivos
    //caso use wordpress mude para .pipe(gulp.dest('/')) e no comentario do css adicine o ! depois do /*! theme.... */
        .pipe(gulp.dest('public/css/'))    
    //atualizar browser apos mudar o css
        .pipe(browserSync.stream());    
}
//tarefa dada para a função scss
gulp.task('sass',compilaSass);

//funcao para juntar javascripts. Lembrar de envolvelos em { }
function gulpJS(){
    //entra nas pastas do js
    //caso queira em uma ordem especifica é só concatenar ['js1.js','js2.js']
    return gulp.src('public/js/main/**/*.js')    
    //transforma todos em um unico arquivo chamado main.js
    .pipe(concate('main.js'))
    //transformar o codigo novo em antigo para suportar navegadores antigos
    .pipe(babel({presets:['env']}))
    //minimiza o js
    .pipe(uglify())
    //Envia para a pasta de destino
    .pipe(gulp.dest('public/js/'))
    //reload do navegador
    .pipe(browserSync.stream())

}
//chamar gulpJS
gulp.task('mainjs',gulpJS);



//Lidar com bibliotecas de javascript externas.
// Esses aqui se usam os arquivos que já passaram por compressao. tipo o pooper.min, mdb.min, bootstrap.min

function pluginJS() {
    return gulp
    //retira o jquery no caso do wordpress, tire também do package.json
    .src([
        'node_modules/jquery/dist/jquery.min.js',
        'public/js/plugins/jquery.mask.js',
        'public/js/plugins/popper.js',
        'public/js/plugins/bootstrap.js',
        'public/js/plugins/jqBootstrapValidation.js',
        'node_modules/bootstrap-select/dist/js/bootstrap-select.min.js',
        'public/js/plugins/moment.js',
        'public/js/plugins/datepicker.js'
        
    ])    
    .pipe(concate('plugins.js'))    
    .pipe(gulp.dest('public/js/'))
    .pipe(browserSync.stream())
  }
  
gulp.task('pluginjs', pluginJS);

//function optimize img
function optimg(){
    //vai ao diretorio das imagens
    return gulp
    .src(['public/img/main/**/*.{gif,png,jpg,jpeg,svg}'])
    //otimiza os tipos de imagens
    .pipe(imagemin([
        //gif
        imagemin.gifsicle({interlaced: true}),
        //jpeg
        imagemin.jpegtran({progressive: true}),
        //png
        imagemin.optipng({optimizationLevel: 5}),
        //svg
        imagemin.svgo({
            plugins: [
                {removeViewBox: true},
                {cleanupIDs: false}
            ]
        })
    ]))
    //coloca no diretorio das otimizadas
    .pipe(gulp.dest('public/img/opt/'))
}
//tarefa para otimizar imagem
gulp.task('optimages', optimg);

//function par atualizar o navegador
function browser(){
    browserSync.init({
        //Se for wordpress coloca o endereço do wordpress mesmo tira o server e coloca:
        proxy:'localhost:8000'    
});
}
//tarefa oara atualizar o browser
gulp.task('browser-sync',browser);


//arquivps para muldaça altomatica com gulp watch
function watch(){
    //verificar sempre o sass
    gulp.watch('public/css/scss/**/*.scss', compilaSass);
    //sempre verificar o js
    gulp.watch(['public/js/main/*.js'], gulpJS);
    //sempre verificar plugins colocados na pasta
    gulp.watch(['public/js/pluguins/*.js'], pluginJS);
    //verificar sempre o html ou php    
    gulp.watch(['app/Http/Controllers/*.php','resources/**/**/*.php'/* no caso do wordpress add /** /*.php */]).on('change',browserSync.reload);
    gulp.watch(['public/img/main/**/*.{gif,png,jpg,jpeg,svg}'], optimg);
}
//Tarfa de assistir para executar ao salvar
gulp.task ('watch',watch);
//tarefa default do gulp que inicia o watch, browser-sync, sass e main.js
gulp.task ('default',gulp.parallel('watch','browser-sync','sass','mainjs','pluginjs'));