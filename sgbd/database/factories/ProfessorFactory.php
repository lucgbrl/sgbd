<?php

use Faker\Generator as Faker;
use sgbd\Professor;

$factory->define(Professor::class, function (Faker $faker) {
    return [
        'matricula_professor'=>$faker->unique()->numberBetween(10**4,10**5-1),
        'curso_cod_curso'=>$faker->randomElement($array = ['CEC','CEE','CPS','CCE','CFC','CMC','CMD']),
        'regime_trabalho' => $faker->randomElement($array = ['20h','40h','DE']),
    ];
});
