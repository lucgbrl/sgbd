<?php

use Faker\Generator as Faker;
use sgbd\Funcionarios;

$factory->define(Funcionarios::class, function (Faker $faker) {
    return [
        'matricula_funcionario'=>$faker->unique()->numberBetween(10**4,10**5-1),
        'cod_curso'=>$faker->randomElement($array = ['CEC','CEE','CPS','CCE','CFC','CMC','CMD']),       
    ];
});
