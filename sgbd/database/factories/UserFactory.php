<?php

use Faker\Generator as Faker;
use Faker\Provider\pt_BR;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/


$factory->define(sgbd\User::class, function (Faker $faker) {
    //$faker->addProvider(new Faker\Provider\pt_BR\Address($faker));
    //$faker = Faker\Factory::create("pt_BR");
    //$faker = new Faker\Generator();    
    //$faker->addProvider(new Faker\Provider\pt_BR\Person($faker));
    //$faker->addProvider(new Faker\Provider\pt_BR\PhoneNumber($faker));
    return [
        'name' => $faker->name,
        'username' => $faker->unique()->userName,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
        'endereço' => $faker->streetAddress,
        'bairro' => $faker->state,
        'cidade' => $faker->city,
        'estado' => $faker->state,
        'telefone' => $faker->phone,
        'cpf' => $faker->unique()->cpf(false),
        //'tipo_usuario' => 'aluno',
        'cep' => $faker->postcode,
    ];
});
