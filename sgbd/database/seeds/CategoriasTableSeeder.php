<?php

use Illuminate\Database\Seeder;
use sgbd\Categorias;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Categorias::insert([
            [        
              'nome' => 'Aventura',
              'descricao' =>  'Aventura é um gênero onde os personagens enfrentam uma série de obstáculos e situações que fogem ao cotidiano.',
            ],
            [        
                'nome' => 'Ação',
                'descricao' =>  'Ação é um gênero que geralmente envolve uma história de protagonistas do bem contra antagonistas do mal, que resolvem suas disputas com o uso de força física.',
            ],
            [        
                'nome' => 'Comédia',
                'descricao' =>  'Comédia é um gênero onde há frequente uso de humor ou pretende-se provocar o riso nos leitores.',           
            ],
            [        
                'nome' =>  'Comédia',
                'descricao' => 'Comédia é um gênero onde há frequente uso de humor ou pretende-se provocar o riso nos leitores.',            
            ],
            [        
                'nome' =>  'Comédia dramática', 
                'descricao' => 'Comédia dramática é a união dos gêneros comédia e drama. Estas obras geralmente apresentam uma história séria, porém abordada de forma humorística.',           
            ],
            [        
                'nome' => 'Comédia romântica',
                'descricao' => 'Comédia romântica é a união dos gêneros comédia e romance. Estas obras geralmente apresentam uma história romântica, porém abordada de forma humorística.',            
            ],
            [        
                'nome' => 'Drama', 
                'descricao' =>   'Drama é um gênero onde a obra possui caráter sério e apresenta um desenvolvimento de fatos e circunstâncias compatíveis com os da vida real.',          
            ],
            [        
                'nome' =>   'Ficção científica',
                'descricao' =>  'Ficção científica é um gênero que utiliza a ficção científica e a ficção especulativa com base científica de fenômenos que não são totalmente aceitos pela ciência moderna.',          
            ],
            [        
                'nome' => 'Guerra', 
                'descricao' => 'Guerra é um gênero onde se retrata histórias fictícias ou baseadas em fatos reais em um contexto de confrontos históricos.',           
            ],
            [        
                'nome' =>  'Policial',
                'descricao' => 'Policial é um gênero onde a obra envolve crimes e criminosos, policiais e detectives particulares, mafiosos e ladrões.',           
            ],
            [        
                'nome' => 'Romance',
                'descricao' => 'Romance é um gênero onde o enredo se desenvolve em torno de um envolvimento amoroso entre os protagonistas.',         
            ],  
            [        
                'nome' =>    'Terror', 
                'descricao' => 'Terror é um gênero onde se procura uma reação emocional negativa dos espectadores, ao jogar com os medos primários da audiência.',           
            ], 
            [        
                'nome' =>  'Documentário',
                'descricao' =>  'Documentário é um gênero não-ficcional e que se caracteriza principalmente pelo compromisso da exploração da realidade.',          
            ],
            [        
                'nome' => 'Engenharia da Computação',
                'descricao' => 'Livros acadêmicos voltados para a área de estudos da Engenharia da Computação.',         
            ],
            [        
                'nome' => 'Psicologia',
                'descricao' => 'Livros acadêmicos voltados para a área de estudos da Pisocologia.'  ,         
            ],
            [        
                'nome' => 'Física',
                'descricao' =>    'Livros acadêmicos voltados para a área de estudos da Física.',          
            ],
            [        
                'nome' => 'Química',
                'descricao' => 'Livros acadêmicos voltados para a área de estudos da Química.',         
            ],  
            [        
                'nome' => 'Matemática',
                'descricao' =>  'Livros acadêmicos voltados para a área de estudos da Matemática.',      
            ],  
            [        
                'nome' =>  'Astronomia',
                'descricao' => 'Livros acadêmicos voltados para a área de estudos da Astronomia.',         
            ],  
            [        
                'nome' => 'Administração',
                'descricao' =>  'Livros acadêmicos voltados para a área de estudos da Administração.',           
            ],  
            [        
                'nome' => 'Estatística',
                'descricao' => 'Livros acadêmicos voltados para a área de estudos de Estatística.',           
            ],  
            [        
                'nome' => 'Mangá',
                'descricao' => 'Histórias em quadrinhos de origem japonesa.',           
            ]
        ]);
    }
}
