<?php

use Illuminate\Database\Seeder;
use sgbd\Biblio;
use Illuminate\Support\Facades\Hash;


class BiblioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Biblio::insert([
            [
                'email' => 'fwesleym@gmail.com',
                'name' => 'Francisco Wesley Melo',
                'username' =>'wesleymelo',
                'password' => Hash::make('password'),
            ]
        ]);
    }
}
