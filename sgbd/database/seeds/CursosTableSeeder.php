<?php

use Illuminate\Database\Seeder;
use sgbd\Cursos;

class CursosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cursos::insert([

            [
                'cod_curso' => 'CEC',
                'nome_curso' => 'Engenharia de Computação',  
            ],
            [
                'cod_curso' => 'CEE',
                'nome_curso' => 'Engenharia Elétrica',
            ],
            [
                'cod_curso' => 'CPS',
                'nome_curso' => 'Psicologia', 
            ],
            [
                'cod_curso' => 'CCE',
                'nome_curso' => 'Ciências Economicas',
            ],
            [
                'cod_curso' => 'CFC',
                'nome_curso' => 'Finanças',   
            ],
            [
                'cod_curso' => 'CMC',
                'nome_curso' => 'Música',
            ],
            [
                'cod_curso' => 'CMD',
                'nome_curso' => 'Medicina', 
            ]
        ]);
    }

}
