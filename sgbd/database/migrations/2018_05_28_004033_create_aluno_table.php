<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlunoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aluno', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('matricula_aluno')->primary();
            $table->timestamp('data_ingreso')->nullable(true)->useCurrent();
            $table->dateTime('data_conclusao_prevista')->nullable(true);
            $table->string('curso_cod_curso',3);
            $table->integer('id_user')->unsigned();
            $table->foreign('curso_cod_curso')->references('cod_curso')->on('curso')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');


        });
            //triggers
            DB::unprepared(
                /** lang Mysql */
                '
                CREATE TRIGGER `aluno_BINS`
                BEFORE INSERT ON `aluno`
                FOR EACH ROW
                BEGIN
                  IF NEW.data_conclusao_prevista IS NULL THEN
                    IF NEW.data_ingreso IS NULL THEN
  				            SET NEW.data_conclusao_prevista = DATE_ADD(NOW(), INTERVAL 7 YEAR);
                    ELSE
                      SET NEW.data_conclusao_prevista = DATE_ADD(NEW.data_ingreso, INTERVAL 7 YEAR);
                    END IF;
  			          ELSE
  				            SIGNAL sqlstate \'45001\' set message_text = \'A Data deve ser Nula\';
  			          END IF;
                    END');
                    
                    DB::unprepared('
                    CREATE TRIGGER `aluno_BUPD`
                    BEFORE UPDATE ON `aluno`
                    FOR EACH ROW
                    BEGIN
                        IF NEW.data_ingreso IS NULL THEN
                            SET NEW.data_conclusao_prevista = DATE_ADD(NOW(), INTERVAL 7 YEAR);
                        ELSE
                            SET NEW.data_conclusao_prevista = DATE_ADD(NEW.data_ingreso, INTERVAL 7 YEAR);
                        END IF;
                    END');
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aluno');
    }
}
