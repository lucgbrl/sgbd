<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLivroHasAutoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('livro_has_autores', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('livro_ISBN',45);
            $table->integer('autores_cpf')->unsigned();
            $table->foreign('livro_ISBN')->references('ISBN')->on('livro')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('autores_cpf')->references('cpf')->on('autor')->onDelete('cascade')->onUpdate('cascade');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('livro_has_autores');
    }
}
