<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLivroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('livro', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('ISBN',45)->primary();
            $table->string('titulo',255);
            $table->year('ano_lancamento',4);
            $table->string('editora',45);
            $table->integer('quantidade_copias')->unsigned();;
            $table->integer('id_categoria')->unsigned();
            $table->integer('disponiveis')->nullable(true)->default(0);
            $table->foreign('id_categoria')->references('id_categoria')->on('categoria')->onDelete('cascade')->onUpdate('cascade');
        });
        //Triggers
        DB::unprepared(
            /** lang Mysql */
            '
            CREATE TRIGGER `livro_BINS`
            BEFORE INSERT ON `livro`
            FOR EACH ROW
            BEGIN
                IF NOT NEW.disponiveis THEN
                        SET NEW.disponiveis = new.quantidade_copias;
                else
                        SIGNAL sqlstate \'45001\' set message_text = \'Quantidade de copias nula\';
                END IF;
                END '
            );
            DB::unprepared(
                /** lang Mysql */
                '
                CREATE TRIGGER `livro_BUPD`
                 BEFORE UPDATE ON `livro`
                 FOR EACH ROW
                 BEGIN
                 SET @quantidade_copias = old.quantidade_copias;
                 SET @quantidade_copias_emprestimo = (SELECT count(ISBN) FROM emprestimo WHERE ISBN = new.ISBN);
                 SET @disponiveis = @quantidade_copias - @quantidade_copias_emprestimo;
                 IF new.quantidade_copias >= @disponiveis THEN
                    SET new.disponiveis = new.quantidade_copias - @quantidade_copias_emprestimo;
                 ELSE
                    IF  (SIGN(new.quantidade_copias - @quantidade_copias_emprestimo) = -1 ) THEN
                      SIGNAL sqlstate \'45001\' set message_text = \'Quantidade de copias invalida\';
                    ELSE
                 SET new.disponiveis = new.quantidade_copias - @quantidade_copias_emprestimo;
                    END IF;
                END IF;
                END'
            );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('livro');
    }
}
