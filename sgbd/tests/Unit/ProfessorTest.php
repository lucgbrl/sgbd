<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use sgbd\User;
use sgbd\Professor;

class ProfessorTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {   
        factory(User::class,20)->create(['tipo_usuario' => 'professor'])->each(function ($c) {            
            $c->professor()->save(factory(Professor::class)->make());       
        });
        $this->assertTrue(true);
    }
}
