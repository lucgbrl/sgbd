<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use sgbd\User;
use sgbd\Funcionarios;

class FuncionariosTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        factory(User::class,20)->create(['tipo_usuario' => 'funcionario'])->each(function ($c) {            
            $c->funcionario()->save(factory(Funcionarios::class)->make());       
        });
        $this->assertTrue(true);
    }
}
