<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use sgbd\User;
use sgbd\Aluno;

class AlunoTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        factory(User::class,20)->create(['tipo_usuario' => 'aluno'])->each(function ($c) {            
            $c->aluno()->save(factory(Aluno::class)->make());       
        });
        $this->assertTrue(true);
    }
}
